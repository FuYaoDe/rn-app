## Included

* [React](https://github.com/facebook/react) & [React Native](https://github.com/facebook/react-native) v0.42
* [Redux](https://github.com/reactjs/redux) & [Remote Redux DevTools](https://github.com/zalmoxisus/remote-redux-devtools) & [On Debugger](https://github.com/jhen0409/remote-redux-devtools-on-debugger)
* [Immutable](https://github.com/facebook/immutable-js) & [Immutable DevTools](https://github.com/andrewdavey/immutable-devtools)
* [Babel](https://github.com/babel/babel) & Plugins: [transform-decorators-legacy](https://github.com/loganfsmyth/babel-plugin-transform-decorators-legacy)
* [react-native-router-flux](https://github.com/aksonov/react-native-router-flux) v3.38.0

## 開發環境

### [install nvm](https://github.com/creationix/nvm#installation)
```
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.2/install.sh | bash
```

The script clones the nvm repository to ~/.nvm and adds the source line to your profile (~/.bash_profile, ~/.zshrc, ~/.profile, or ~/.bashrc).

```
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh" # This loads nvm
```

安裝完後重啟 terminal 或 `source ~/.bashrc`

### install

```
nvm install 6.9.1
nvm alias default 6.9.1
npm install -g react-native-cli
tar xvf gec-app.tar
cd gec-app
npm run ios-bundle
npm run android-bundle
```


## Config
`src/config/index.js`


## Installation

See [Getting Started](https://facebook.github.io/react-native/docs/getting-started.html) to install requirement tools.

```bash
$ npm install -g react-native-cli
$ npm install
```

Also, you can use [generator-rnb](https://github.com/jhen0409/generator-rnb) to create project with this boilerplate.

## Development

#### Start local server

```bash
$ npm start
```

#### iOS

Run command to open iOS simulator and run app:

```bash
$ npm run ios
```

Or open `ios/RNBoilerplate.xcodeproj` file with XCode:

```bash
$ npm run ios-open
```

#### Android (5.0+)

Open Android emulator (recommended [Genymotion](https://www.genymotion.com)) and run command: (Or connect real device via USB)

```bash
$ npm run android
```

## DevTools

In development mode, you can install [React Native Debugger](https://github.com/jhen0409/react-native-debugger) as default debugger. if not install, it will use [Remote Redux DevTools](https://github.com/zalmoxisus/remote-redux-devtools) and [On Debugger](https://github.com/jhen0409/remote-redux-devtools-on-debugger).

## Test

We used [react-native-mock](https://github.com/lelandrichardson/react-native-mock), and test with [Mocha](https://github.com/mochajs/mocha), [Enzyme](https://github.com/airbnb/enzyme).

```bash
$ npm test
```


## troubleshooting

### Error watching file for changes: EMFILE

<https://github.com/facebook/react-native/issues/910#issuecomment-300505599>


# Release

## Android 

~/.gradle/gradle.properties 
寫入
```
KBRO_DEBUG_RELEASE_STORE_FILE=kbro-debug-release-key.keystore
KBRO_DEBUG_RELEASE_KEY_ALIAS=kbro-debug
KBRO_DEBUG_RELEASE_STORE_PASSWORD=kbro1234
KBRO_DEBUG_RELEASE_KEY_PASSWORD=kbro1234
```


# Troubleshooting

**'AppDelegate.h' file not found RNVideoPlayer.h**
or
`node_modules/react-native-native-video-player/ios/RNVideoPlayer/RNVideoPlayer.h:9:9: fatal error: 'AppDelegate.h' file not found #import "AppDelegate.h"`

/Libraries/RNVideoPlayer > Build Settings > Search Paths > Header Search Paths`Edit the path variable from $(SRCROOT)/../../../HuntersLog to $(SRCROOT)/../../../RNBoilerplate`


**Error: more than one library with package name 'com.wix.reactnativenotifications'**
`android/app/src/main/java/com/kbro/healthcare/MainApplication.java`
註解 new RNNotificationsPackage()

**8.0 開不起來
RNSound target 改為 iOS 8.0


#Argument list too long
file > project setting > advanced > custom > relative to workspace 確認下方的 products build/Build/Products



## FCM
客製部分
https://github.com/trunksys/react-native-fcm/commit/704186a93fc218f8e5035eb94b7f487bb5bddbc3

server 推播過來的資料
```
{
  alert: '資料'
}
```
data.get("alert");
如果推播 server 能傳
```
{
  "to":"FCM_TOKEN",
  "data": {
    "type":"MEASURE_CHANGE",
    "custom_notification": {
      "body": "test body",
      "title": "test title",
      "color":"#00ACD4",
      "priority":"high",
      "icon":"ic_notif",
      "group": "GROUP",
      "id": "id",
      "show_in_foreground": true
    }
  }
}
```
就不用額外使用這個客製的 repo，能直接使用 react-native-fcm

# Code Push

拿到金鑰
```
code-push deployment ls ${app_name} -k
```
app_name:
- gec
- gec-android