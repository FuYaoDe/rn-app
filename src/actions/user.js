export const RECEIVED_UPDATE_USER = 'RECEIVED_UPDATE_USER';
export const CLEAN_UPDATE_USER = 'CLEAN_UPDATE_USER';

export function updateUser(data) {
  return {
    type: RECEIVED_UPDATE_USER,
    data,
  };
}

export function cleanUser() {
  return {
    type: CLEAN_UPDATE_USER,
  };
}
