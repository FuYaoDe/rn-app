export const RECEIVED_UPDATE_TIMER = 'RECEIVED_UPDATE_TIMER';
export const COUNTDOWN_TIME = 300;


export function updateTimer(key, data) {
  return {
    type: RECEIVED_UPDATE_TIMER,
    key,
    data,
  };
}

export function resetCalendar(key) {
  return {
    type: RECEIVED_UPDATE_TIMER,
    key,
    data: {
      countdownTime: COUNTDOWN_TIME,
      startTime: null,
    },
  };
}
