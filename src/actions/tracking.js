export const RECEIVED_UPDATE_TRACKING = 'RECEIVED_UPDATE_TRACKING';
export const CLEAN_TRACKING = 'CLEAN_TRACKING';

export function updateTracking(data) {
  return {
    type: RECEIVED_UPDATE_TRACKING,
    data,
  };
}
export function cleanTracking() {
  return {
    type: CLEAN_TRACKING,
  };
}
