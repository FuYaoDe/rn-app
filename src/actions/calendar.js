import moment from 'moment';
import shortid from 'shortid';

export const RECEIVED_UPDATE_CALENDAR = 'RECEIVED_UPDATE_CALENDAR';

export function updateCalendar(data) {
  return {
    type: RECEIVED_UPDATE_CALENDAR,
    data,
  };
}

export function resetCalendar() {
  return {
    type: RECEIVED_UPDATE_CALENDAR,
    data: {
      isOpen: false,
      selectedStartDate: null,
      selectedEndDate: null,
      reset: shortid.generate(),
    },
  };
}
