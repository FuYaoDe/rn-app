import { combineReducers } from 'redux';
import alert from './alert';
import calendar from './calendar';
import loading from './loading';
import notifyAction from './notifyAction';
import routes from './routes';
import user from './user';
import verificationTimer from './verificationTimer';

export default combineReducers({
  alert,
  calendar,
  loading,
  notifyAction,
  routes,
  user,
  verificationTimer,
});
