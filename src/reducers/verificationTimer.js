import { RECEIVED_UPDATE_TIMER, COUNTDOWN_TIME } from '../actions/verificationTimer';


export const initialState = {
  forgetPassword: {
    countdownTime: COUNTDOWN_TIME,
    startTime: null,
    id: null,
  },
};

export const ACTION_HANDLERS = {
  [RECEIVED_UPDATE_TIMER]: (state = {}, action) => {
    const updateNewState = { ...state };
    updateNewState[action.key] = {
      ...updateNewState[action.key],
      ...action.data,
    };
    return updateNewState;
  },
};

export default function (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
}
