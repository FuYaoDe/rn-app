import { RECEIVED_UPDATE_USER, CLEAN_UPDATE_USER } from '../actions/user';


export const userDataInitialState = {
  frontUpdateTime: null,
  student: {},
  parent: {},
};

export const ACTION_HANDLERS = {
  [RECEIVED_UPDATE_USER]: (state = {}, action) => ({ ...state, ...action.data, frontUpdateTime: new Date() }),
  [CLEAN_UPDATE_USER]: (state = {}, action) => ({ frontUpdateTime: null, student: {}, parent: {} })
};

export default function (state = userDataInitialState, action) {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
}
