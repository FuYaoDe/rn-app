import moment from 'moment';
import { RECEIVED_UPDATE_CALENDAR } from '../actions/calendar';


const initialState = {
  isOpen: false,
  selectedStartDate: null,
  selectedEndDate: null,
  reset: '1',
};

export const ACTION_HANDLERS = {
  [RECEIVED_UPDATE_CALENDAR]: (state = {}, action) => ({ ...state, ...action.data }),
};

export default function (state = initialState, action) {
  const handler = ACTION_HANDLERS[action.type];
  return handler ? handler(state, action) : state;
}
