import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View, Image } from 'react-native';
import RoundButton from '../components/Button';
import colors from '../config/color';
import Screen from '../utils/screen';
import images from '../config/image';

const styles = StyleSheet.create({
  primary: {
    // borderRadius: Screen.moderateScale(20),
    marginBottom: Screen.moderateScale(20),
    maxWidth: Screen.width - Screen.moderateScale(32),
    height: Screen.moderateScale(50),
    width: Screen.moderateScale(115)
  },
  sub: {
    marginBottom: Screen.moderateScale(20),
    maxWidth: Screen.width - Screen.moderateScale(32),
    height: Screen.moderateScale(50),
    width: Screen.moderateScale(115)
  }
});

export function PrimaryBtn({ style, ...props }) {
  return (
    <RoundButton
      style={[styles.primary, style]}
      btnColor={props.disablePress ? colors.silver : colors.mainBlue}
      textColor={colors.whiteThree}
      textStyle={{ fontSize: Screen.moderateScale(16) }}
      {...props}
    />
  );
}

export function SubBtn({ style, ...props }) {
  return (
    <RoundButton
      style={[styles.sub, style]}
      btnColor={colors.whiteThree}
      textColor={props.disablePress ? colors.silver : colors.mainBlue}
      borderColor={props.disablePress ? colors.silver : colors.mainBlue}
      textStyle={{ fontSize: Screen.moderateScale(18) }}
      {...props}
    />
  );
}

export function LineBtn({ style, ...props }) {
  return (
    <TouchableOpacity
      hitSlop={{
        top: 30,
        left: 5,
        bottom: 30,
        right: 5
      }}
      style={{
        borderBottomColor: colors.mainBlue,
        borderBottomWidth: Screen.onePixel * 2
      }}
      onPress={props.onPress}
    >
      <Text
        style={{ fontSize: Screen.moderateScale(12), color: colors.subBlue }}
      >
        {props.text}
      </Text>
    </TouchableOpacity>
  );
}

export function LogoBtn({ style, ...props }) {
  return (
    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
      <Image style={{ width: Screen.moderateScale(150), height: Screen.moderateScale(50) }} resizeMode="contain" source={images.big_logo} />
      <RoundButton
        style={[styles.primary, style]}
        style={[styles.primary, style]}
        btnColor={props.disablePress ? colors.silver : colors.mainBlue}
        textColor={colors.whiteThree}
        textStyle={{ fontSize: Screen.moderateScale(16) }}
        textStyle={{ fontSize: Screen.moderateScale(16) }}
        {...props}
      />
    </View>
  );
}


export function SelectBtn({ style, ...props }) {
  let selectColor = {};
  if (props.onSelect) {
    selectColor = {
      textColor: colors.whiteThree,
      btnColor: colors.mainBlue,
      borderColor: colors.mainBlue
    };
  } else {
    selectColor = {
      btnColor: colors.silver,
      textColor: colors.whiteThree,
      borderColor: colors.silver
    };
  }
  return (
    <RoundButton
      style={[styles.primary, style]}
      textStyle={{ fontSize: Screen.moderateScale(16) }}
      {...selectColor}
      {...props}
    />
  );
}
