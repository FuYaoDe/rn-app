import React from 'react';
import { StyleSheet, Text } from 'react-native';
import RoundButton from '../components/Button';
import colors from '../config/color';
import Screen from '../utils/screen';


const styles = StyleSheet.create({
  primary: {
    borderRadius: Screen.moderateScale(20),
    marginBottom: Screen.moderateScale(20),
    maxWidth: Screen.width - Screen.moderateScale(32)
  },
  sub: {
    borderRadius: Screen.moderateScale(20),
    maxWidth: Screen.width - Screen.moderateScale(32)
  },
  select: {
    borderRadius: Screen.moderateScale(20),
    marginRight: Screen.moderateScale(10),
    minWidth: 100,
    height: Screen.moderateScale(30),
    marginBottom: Screen.moderateScale(10)
  },
  selectRect: {
    borderRadius: Screen.moderateScale(20),
    minWidth: 100,
    height: Screen.moderateScale(30),
    marginBottom: Screen.moderateScale(10)
  },
  small: {
    borderRadius: Screen.moderateScale(20),
    minWidth: 100,
    height: Screen.moderateScale(30)
  },
  bage: {
    borderRadius: Screen.moderateScale(20),
    height: Screen.moderateScale(20),
    marginBottom: Screen.moderateScale(3)
  },
  detail: {
    borderRadius: Screen.moderateScale(20),
    height: Screen.moderateScale(35),
    maxWidth: Screen.width - Screen.moderateScale(32),
    borderRadius: 0,
    paddingTop: Screen.moderateScale(9),
    paddingBottom: Screen.moderateScale(8),
    justifyContent: 'center'
  }
});

export function PrimaryBtn({ style, ...props }) {
  return (
    <RoundButton
      style={[styles.primary, style]}
      btnColor={colors.turquoise}
      textColor={colors.whiteThree}
      {...props}
    />
  );
}
export function DetailBtn({ style, ...props }) {
  return (
    <RoundButton
      style={[styles.detail, style]}
      btnColor={colors.whiteThree}
      textColor={colors.purpleyGrey}
      borderColor="#979797"
      {...props}
    />
  );
}
export function SubBtn({ style, ...props }) {
  return (
    <RoundButton
      style={[styles.sub, style]}
      btnColor={colors.whiteThree}
      textColor={colors.turquoise}
      borderColor={colors.turquoise}
      textStyle={{ fontSize: Screen.moderateScale(18) }}
      {...props}
    />
  );
}

export function SelectBtn({ style, ...props }) {
  let selectColor = {};
  if (props.onSelect) {
    selectColor = {
      textColor: colors.whiteThree,
      btnColor: colors.darkSkyBlue,
      borderColor: colors.darkSkyBlue
    };
  } else {
    selectColor = {
      btnColor: colors.whiteThree,
      textColor: colors.purpleyGrey,
      borderColor: colors.coolGrey
    };
  }
  return (
    <RoundButton
      style={[styles.select, style]}
      textStyle={{ fontSize: Screen.moderateScale(16) }}
      {...selectColor}
      {...props}
    />
  );
}

export function SelectRectBtn({ style, ...props }) {
  let selectColor = {};
  if (props.onSelect) {
    selectColor = {
      textColor: colors.whiteThree,
      btnColor: colors.tealish,
      borderColor: colors.tealish
    };
  } else {
    selectColor = {
      btnColor: colors.whiteThree,
      textColor: colors.tealish,
      borderColor: colors.tealish
    };
  }
  return (
    <RoundButton
      style={[
        styles.selectRect,
        {
          borderRadius: 0,
          margin: 0
        },
        props.borderRight ? {} : { borderRightWidth: 0 },
        style
      ]}
      textStyle={{ fontSize: Screen.moderateScale(7 * 2) }}
      {...selectColor}
      {...props}
    />
  );
}

export function SmallBtn({ style, ...props }) {
  return (
    <RoundButton
      style={[styles.small, style]}
      textStyle={{ fontSize: Screen.moderateScale(16) }}
      btnColor={colors.purpleyGrey}
      textColor={colors.whiteThree}
      borderColor={colors.purpleyGrey}
      {...props}
    />
  );
}

export function Bage({ style, ...props }) {
  return (
    <RoundButton
      style={[styles.bage, style]}
      textStyle={{ fontSize: Screen.moderateScale(14), fontWeight: '500' }}
      btnColor={colors.whiteThree}
      textColor={props.color}
      borderColor={props.color}
      onPress={props.onPress}
      {...props}
    />
  );
}
