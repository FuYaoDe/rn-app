import React, { Component, PropTypes } from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  Image,
  View,
  Platform,
  Text,
} from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import moment from 'moment';
import colors from '../config/color';
import { updateCalendar, resetCalendar } from '../actions/calendar';
import { H2 } from './Label';
import Screen from '../utils/screen';

const calendarImage = require('../assets/icon/Calendar.png');
const closeImage = require('../assets/icon/close_2.png');

const styles = StyleSheet.create({
  preview: {
    margin: 10,
    marginBottom: 0,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    backgroundColor: colors.whiteThree,
    alignSelf: 'stretch',
    height: Screen.moderateScale(32.5 * 2),
    borderRadius: Screen.moderateScale(2.5)
  },
  dateStartBlock: {
    alignItems: 'center',
    zIndex: 1,
  },
  titleDateText: {
    height: Screen.moderateScale(11.5 * 2),
    fontSize: Screen.moderateScale(10 * 2),
    fontWeight: '300',
    textAlign: 'center',
    color: colors.tealish
  },
  titleYearText: {
    height: Screen.moderateScale(8.5 * 2),
    fontSize: Screen.moderateScale(7 * 2),
    textAlign: 'center',
    color: colors.greyishBrown
  },
  titleMiddleText: {
    height: Screen.moderateScale((9 * 2) + 4),
    fontSize: Screen.moderateScale(9 * 2),
    textAlign: 'center',
    color: colors.silver,
    paddingLeft: Screen.moderateScale(20),
    paddingRight: Screen.moderateScale(20)
  },
  timeIcon: {
    width: Screen.moderateScale(18),
    height: Screen.moderateScale(18),
    ...Platform.select({
      android: {
        marginTop: Screen.moderateScale(4)
      }
    })
  },
});
const WEEKDAYS_ZH = ['日', '一', '二', '三', '四', '五', '六'];

@connect(
  state => ({
    calendar: state.calendar,
  }),
  dispatch => bindActionCreators({
    updateCalendar,
    resetCalendar,
  }, dispatch)
)
export default class CalendarButton extends Component {
  static propTypes = {
    selectedStartDate: PropTypes.any,
    selectedEndDate: PropTypes.any,
    reset: PropTypes.func,
  };
  static defaultProps = {
    selectedStartDate: null,
    selectedEndDate: null,
    reset: () => {},
  };
  constructor(props) {
    super(props);
    this.state = {};
  }

  openCalendarPicker = () => {
    this.props.updateCalendar({
      isOpen: true,
    });
  };

  render() {
    const {
      selectedStartDate, selectedEndDate, reset, resetCalendar
    } = this.props;

    let startY = '';
    let startDate = '-';
    let startWeek = '';
    let endY = '';
    let endDate = '-';
    let endWeek = '';

    if (selectedStartDate) {
      const selectedStart = moment(selectedStartDate);
      startY = selectedStart.format('YYYY');
      startDate = selectedStart.format('MM/DD');
      startWeek = `(週${WEEKDAYS_ZH[selectedStart.format('e')]})`;
    }
    if (selectedEndDate) {
      const selectedEnd = moment(selectedEndDate);
      endY = selectedEnd.format('YYYY');
      endDate = selectedEnd.format('MM/DD');
      endWeek = `(週${WEEKDAYS_ZH[selectedEnd.format('e')]})`;
    }

    return (
      <View style={styles.preview}>
        <TouchableOpacity
          onPress={this.openCalendarPicker}
          style={{
            paddingLeft: Screen.moderateScale(10),
            flex: 1,
            alignSelf: 'stretch',
            alignItems: 'center',
            justifyContent: 'center'
          }}
        >
          {selectedStartDate || selectedEndDate
            ? <View
              style={{
                alignSelf: 'stretch',
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-around'
              }}
            >
              <View style={styles.dateStartBlock}>
                <Text style={styles.titleYearText}>
                  {startY}
                </Text>
                <Text style={styles.titleDateText}>
                  {`${startDate}${startWeek}`}
                </Text>
              </View>
              <Text style={styles.titleMiddleText}>至</Text>
              <View style={styles.dateStartBlock}>
                <Text style={styles.titleYearText}>
                  {endY}
                </Text>
                <Text style={styles.titleDateText}>
                  {`${endDate}${endWeek}`}
                </Text>
              </View>
            </View>
            : <View style={{ flexDirection: 'row' }}>
              <Image
                source={calendarImage}
                resizeMode="contain"
                style={styles.timeIcon}
              />
              <H2 style={{ paddingLeft: Screen.moderateScale(10) }}>時間篩選</H2>
            </View>}
        </TouchableOpacity>
        {selectedStartDate || selectedEndDate
          ? <TouchableOpacity
            onPress={() => {
              reset();
              resetCalendar();
            }}
            style={{ padding: Screen.moderateScale(13) }}
          >
            <Image
              source={closeImage}
              resizeMode="contain"
              style={styles.timeIcon}
            />
          </TouchableOpacity>
          : null}
      </View>
    );
  }
}
