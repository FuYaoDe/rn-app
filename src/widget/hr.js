import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import RoundButton from '../components/Button';
import colors from '../config/color';
import { H4 } from './Label';
import Screen from '../utils/screen';

const styles = StyleSheet.create({
  hr1: {
    height: Screen.moderateScale(50),
    width: Screen.width,
    paddingLeft: Screen.moderateScale(16),
    paddingBottom: Screen.moderateScale(10),
    backgroundColor: colors.white,
    justifyContent: 'flex-end'
  }
});

export default function HR({ style, ...props }) {
  return (
    <View style={[styles.hr1, style]}>
      <H4
        style={[{ color: props.textColor || colors.coolGrey }, props.textStyle]}
      >
        {props.text}
      </H4>
    </View>
  );
}
