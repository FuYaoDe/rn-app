import React, { PropTypes } from 'react';
import { StyleSheet, View, Image } from 'react-native';
import { H1, H3 } from './Label';
import Screen from '../utils/screen';

const styles = StyleSheet.create({
  container: {
    flex: 0.56,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: Screen.moderateScale(24),
    marginTop: Screen.moderateScale(15),
  },
  subTitle: {
    paddingBottom: Screen.moderateScale(65),
    paddingTop: Screen.moderateScale(15),
    paddingLeft: Screen.moderateScale(44),
    paddingRight: Screen.moderateScale(40),

  }
});

const propTypes = {
  logo: PropTypes.any,
  title: PropTypes.string,
  logoHeight: PropTypes.number.isRequired,
  logoWidth: PropTypes.number.isRequired,
};
const defaultProps = {};

const IconTitle = props => (
  <View style={[styles.container, props.style]}>
    <Image
      source={props.logo}
      style={{ height: props.logoHeight, width: props.logoWidth }}
    />
    <H1 style={styles.title}>{props.title}</H1>
    <H3 style={[styles.subTitle, props.subTitleStyle]}>{props.subTitle}</H3>
  </View>
);

IconTitle.propTypes = propTypes;
IconTitle.defaultProps = defaultProps;
export default IconTitle;
