import React, { PropTypes, Component } from 'react';
import { StyleSheet, Text, Platform, PixelRatio, View, TouchableOpacity, Image } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import InputBox from '../components/InputBox';
import colors from '../config/color';
import Screen from '../utils/screen';
import { H3, DefaultText } from './Label';

const styles = StyleSheet.create({
  primary: {
    borderBottomColor: colors.mainBlue,
    marginBottom: 0,
  },
  sub: {
    // paddingBottom: 5,
    // borderBottomWidth: Screen.onePixel * 2,
  },
  listContent: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomColor: colors.silver,
    borderBottomWidth: Screen.onePixel,
    paddingRight: Screen.moderateScale(15),
    paddingLeft: Screen.moderateScale(15),
    paddingTop: Screen.moderateScale(5),
    paddingBottom: Screen.moderateScale(5),
  },
  list: {
    borderBottomWidth: 0,
    marginBottom: 0,
  },
  primaryInputStyle: {
    ...Platform.select({
      ios: {
        fontSize: Screen.moderateScale(16),
      },
      android: {
        fontSize: Screen.moderateScale(Math.floor(16 / PixelRatio.getFontScale())),
      }
    }),
    marginLeft: 0,
    textAlign: 'left',
    color: colors.mainBlue,
    ...Platform.select({
      ios: {
        height: Screen.moderateScale(35),
      }
    })
  },
  subInputStyle: {
    ...Platform.select({
      ios: {
        fontSize: Screen.moderateScale(16),
      },
      android: {
        fontSize: Screen.moderateScale(Math.floor(16 / PixelRatio.getFontScale())),
      }
    }),
    letterSpacing: -0.22,
    textAlign: 'left',
    color: colors.greyishBrown,
    ...Platform.select({
      ios: {
        height: Screen.moderateScale(35),
      }
    })
  },
  listInputStyle: {
    ...Platform.select({
      ios: {
        fontSize: Screen.moderateScale(16),
      },
      android: {
        fontSize: Screen.moderateScale(16),
      }
    }),
    marginLeft: 0,
    textAlign: 'right',
    color: colors.mainBlue,
    ...Platform.select({
      ios: {
        height: Screen.moderateScale(35),
      }
    })
  },
  listDisableInput: {
    flex: 1,
    paddingLeft: Screen.moderateScale(10),
    paddingTop: Screen.moderateScale(10),
    paddingBottom: Screen.moderateScale(10),
  },
  listDisableInputText: {
    textAlign: 'right',
    fontSize: Screen.moderateScale(12),
    color: colors.mainBlue
  },
});

export class PrimaryInput extends Component {
  static propTypes = {};
  static defaultProps = {};
  constructor(props) {
    super(props);
    this.state = {};
  }

  test = (str) => {
    if (this.input) {
      this.input.test(str);
    }
  }

  check = () => {
    if (this.input) {
      return this.input.onEnd();
    }
  }
  focus = () => {
    if (this.input) {
      return this.input.focus();
    }
  }

  render() {
    return (
      <InputBox
        ref={(ref) => { this.input = ref; }}
        {...this.props}
        inputStyle={[styles.primaryInputStyle, this.props.inputStyle]}
        style={[styles.primary, this.props.style]}
        errorTextStyle={{
          fontSize: Screen.moderateScale(12),
          color: colors.pink
        }}
        errorStyle={{
          container: {
            borderBottomColor: colors.mainBlue,
          },
          text: {
            color: colors.mainBlue
          },
          placeholderTextColor: colors.silver
        }}
      />
    );
  }
}

export class SubInput extends Component {
  static propTypes = {};
  static defaultProps = {};
  constructor(props) {
    super(props);
    this.state = {};
  }

  test = (str) => {
    if (this.input) {
      this.input.test(str);
    }
  }

  check = () => {
    if (this.input) {
      return this.input.onEnd();
    }
  }
  focus = () => {
    if (this.input) {
      this.input.focus();
    }
  }

  render() {
    return (
      <InputBox
        ref={(ref) => { this.input = ref; }}
        labelColor={colors.greyishBrown}
        {...this.props}
        style={[styles.sub, this.props.style]}
        inputStyle={[styles.subInputStyle, this.props.inputStyle]}
      />
    );
  }
}

export class ListInput extends Component {
  static propTypes = {};
  static defaultProps = {
    onPess: null,
    editable: true,
  };
  constructor(props) {
    super(props);
    this.state = {};
  }

  test = (str) => {
    if (this.input) {
      this.input.test(str);
    }
  }

  check = () => {
    if (this.input) {
      return this.input.onEnd();
    }
  }
  focus = () => {
    if (this.input) {
      return this.input.focus();
    }
  }

  render() {
    return (
      <TouchableOpacity
        style={styles.listContent}
        onPress={this.props.onPress}
        activeOpacity={this.props.onPress ? 0.2 : 1}
      >
        <View style={{ justifyContent: 'center' }}>
          <H3>{this.props.title}</H3>
        </View>
        {
          this.props.editable ?
            <View style={{ flex: 1, paddingLeft: Screen.moderateScale(10) }}>
              <InputBox
                ref={(ref) => { this.input = ref; }}
                {...this.props}
                inputStyle={[styles.listInputStyle, this.props.inputStyle]}
                style={[styles.list, this.props.style]}
                errorTextStyle={{
                  fontSize: Screen.moderateScale(12),
                  color: colors.pink
                }}
                errorStyle={{
                  container: {
                    borderBottomColor: colors.mainBlue,
                    marginBottom: 0
                  },
                  text: {
                    color: colors.mainBlue
                  },
                  placeholderTextColor: colors.silver
                }}
              />
            </View> :
            <View style={styles.listDisableInput}>
              <DefaultText style={styles.listDisableInputText}>
                {this.props.value}
              </DefaultText>
            </View>
        }
        <View style={{ justifyContent: 'center', alignItems: 'center', paddingLeft: 6 }}>
          {
            this.props.onPress ?
              <Icon size={Screen.moderateScale(20)} color={colors.silver} name="angle-right" /> :
              <View style={{ width: Screen.moderateScale(this.props.paddingRight ? 8 : 0) }} />
          }
        </View>
      </TouchableOpacity>
    );
  }
}
