import React from 'react';
import { StyleSheet, Text, View, PixelRatio, Platform, TouchableOpacity } from 'react-native';
import CheckBox from 'react-native-check-box';
import colors from '../config/color';
import { H4, DefaultText } from './Label';
import Screen from '../utils/screen';
import _ from 'lodash';

const styles = StyleSheet.create({
  checkBox: {
    justifyContent: 'center',
    alignItems: 'center',
    width: Screen.moderateScale(19),
    height: Screen.moderateScale(19),
    // borderRadius: Screen.moderateScale(2.5 * 2),
    backgroundColor: '#fafafa',
    borderStyle: 'solid',
    borderWidth: Screen.onePixel,
    borderColor: '#d1d1d1',
  },
  checkBoxChecked: {
    justifyContent: 'center',
    alignItems: 'center',
    width: Screen.moderateScale(19),
    height: Screen.moderateScale(19),
    // borderRadius: Screen.moderateScale(2.5 * 2),
    backgroundColor: colors.pink,
    borderStyle: 'solid',
    borderWidth: Screen.onePixel,
    borderColor: colors.pink,
  },
  rightTextStyle: { fontSize: Screen.moderateScale(12), color: colors.subBlue },
});

export default function ({ ...props }) {
  const getText = text => <DefaultText key={text} style={[styles.rightTextStyle, props.rightTextStyle]}>{text}</DefaultText>;
  const renderRightText = () => {
    if (_.isArray(props.rightText)) {
      return props.rightText.map((data) => {
        if (_.isString(data)) {
          return getText(data);
        }
        return data;
      });
    }
    return getText(props.rightText);
  };
  return (
    <CheckBox
      style={[props.style]}
      unCheckedImage={<View style={styles.checkBox} />}
      checkedImage={<View style={styles.checkBoxChecked} />}
      rightTextView={
        <View style={{ marginLeft: Screen.moderateScale(5), flexDirection: 'row', flexWrap: 'wrap' }}>
          {renderRightText()}
          {/*{getText(props.rightText)}*/}
        </View>
      }
      {...props}
    />
  );
}
