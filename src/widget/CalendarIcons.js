import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import CalendarIcon from '../components/CalendarIcon';
import colors from '../config/color';
import moment from 'moment';
import Screen from '../utils/screen';

export function DayCalendarIcon({ style, ...props }) {
  return (
    <CalendarIcon contentStyle={{ width: Screen.moderateScale(47.5 * 2), height: Screen.moderateScale(36 * 2) }} {...props}>
      <Text
        style={{
          fontSize: Screen.moderateScale(18 * 2),
          fontWeight: '300',
          letterSpacing: 0.64 * 2,
          textAlign: 'center',
          color: colors.black
        }}
      >
        {props.day}
      </Text>
      <Text
        style={{
          fontSize: Screen.moderateScale(7 * 2),
          fontWeight: '300',
          letterSpacing: 0.25 * 2,
          textAlign: 'center',
          color: colors.black
        }}
      >
        {props.week}
      </Text>
    </CalendarIcon>
  );
}

export function WeekCalendarIcon({ style, ...props }) {
  return (
    <CalendarIcon
      contentStyle={{
        width: Screen.moderateScale(47.5 * 2),
        height: Screen.moderateScale(47.5 * 2)
      }}
      {...props}
    >
      <Text
        style={{
          fontSize: Screen.moderateScale(12 * 2),
          fontWeight: '300',
          letterSpacing: 0.64 * 2,
          textAlign: 'center',
          color: colors.black
        }}
      >
        {props.startDay}
      </Text>
      <Text
        style={{
          fontSize: Screen.moderateScale(7 * 2),
          fontWeight: '300',
          letterSpacing: 0.43 * 2,
          textAlign: 'center',
          color: colors.coolGrey
        }}
      >
        至
      </Text>
      <Text
        style={{
          fontSize: Screen.moderateScale(12 * 2),
          fontWeight: '300',
          letterSpacing: 0.43 * 2,
          textAlign: 'center',
          color: colors.black
        }}
      >
        {props.endDay}
      </Text>
    </CalendarIcon>
  );
}

export function MonthCalendarIcon({ style, ...props }) {
  return (
    <CalendarIcon contentStyle={{ width: Screen.moderateScale(47.5 * 2), height: Screen.moderateScale(36 * 2) }} {...props}>
      <Text
        style={{
          fontSize: Screen.moderateScale(18 * 2),
          fontWeight: '300',
          letterSpacing: 0.64 * 2,
          textAlign: 'center',
          color: colors.black
        }}
      >
        {props.month}
      </Text>
      <Text
        style={{
          fontSize: Screen.moderateScale(7 * 2),
          fontWeight: '300',
          letterSpacing: 0.25 * 2,
          textAlign: 'center',
          color: colors.black
        }}
      >
        月
      </Text>
    </CalendarIcon>
  );
}
