import React, { PropTypes, Component } from 'react';
import {
  Dimensions,
  TouchableOpacity,
  StyleSheet,
  Text,
  View,
  Animated,
  Easing,
  Platform,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import CalendarPicker from 'react-native-calendar-picker';
import colors from '../config/color';
import BottomButton from '../components/BottomButton';
import moment from 'moment';
import Screen from '../utils/screen';

const MONTHS_ZH = [
  '一月',
  '二月',
  '三月',
  '四月',
  '五月',
  '六月',
  '七月',
  '八月',
  '九月',
  '十月',
  '十一月',
  '十二月'
];
const WEEKDAYS_ZH = ['日', '一', '二', '三', '四', '五', '六'];
const { height, width } = Dimensions.get('window');
const styles = StyleSheet.create({
  mask: {
    height,
    width,
    position: 'absolute',
    backgroundColor: 'rgba(3, 3, 3, 0.6)'
  },
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems: 'stretch',
    paddingBottom: 65
  },
  preview: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    backgroundColor: colors.bluish,
    alignSelf: 'stretch',
    height: Screen.moderateScale(46 * 2),
    zIndex: 2,
  },
  calendar: {
    height: Screen.moderateScale(355)
  },
  bottomButtonText: {
    height: Screen.moderateScale(10.5 * 2),
    fontSize: Screen.moderateScale(9 * 2),
    fontWeight: '500',
    letterSpacing: 0.06,
    textAlign: 'center',
    color: colors.whiteThree
  },
  calendarStyle: {
    backgroundColor: colors.whiteThree
  },
  calendarMarkedDaysColorStyle: {
    backgroundColor: colors.darkSkyBlue,
    color: colors.whiteThree
  },
  dateStartBlock: {
    alignItems: 'center'
  },
  titleDateText: {
    height: Screen.moderateScale(11.5 * 2),
    fontSize: Screen.moderateScale(10 * 2),
    fontWeight: '300',
    textAlign: 'center',
    color: colors.whiteThree
  },
  titleYearText: {
    height: Screen.moderateScale(8.5 * 2),
    fontSize: Screen.moderateScale(7 * 2),
    textAlign: 'center',
    color: colors.whiteThree
  },
  titleMiddleText: {
    height: Screen.moderateScale(9 * 2 + 2),
    fontSize: Screen.moderateScale(9 * 2),
    textAlign: 'center',
    color: colors.whiteThree
  },
  headerStyle: {
    backgroundColor: '#3aa8c8',
    alignSelf: 'stretch',
    height: Screen.moderateScale(25 * 2),
    marginBottom: 0
  },
  headerTextStyle: {
    color: colors.whiteThree
  },
  headerMidTextStyle: {
    fontSize: Screen.moderateScale(12 * 2),
    fontWeight: '500',
    letterSpacing: -0.13 * 2,
    textAlign: 'center',
    color: colors.whiteThree
  },
  headerSideTextStyle: {
    height: Screen.moderateScale(8.5 * 2),
    fontSize: Screen.moderateScale(7 * 2),
    fontWeight: '500',
    letterSpacing: -0.08 * 2,
    textAlign: 'center',
    color: colors.whiteThree
  },
  weekDaysStyle: {
    borderTopWidth: 0,
    borderBottomWidth: 0
  },
  weekDaysTextStyle: {
    fontSize: Screen.moderateScale(9 * 2),
    fontWeight: 'bold',
    letterSpacing: -0.1 * 2,
    textAlign: 'center',
    color: '#2caba6'
  }
});

export default class Calendar extends Component {
  static propTypes = {
    onConfirmClick: PropTypes.func,
    paddingBottom: PropTypes.number,
    onClose: PropTypes.func
  };

  static defaultProps = {
    onConfirmClick: () => {},
    onClose: () => {}
  };

  constructor(props) {
    super(props);
    this.state = {
      selectedStartDate: props.selectedStartDate || null,
      selectedEndDate: props.selectedEndDate || null,
      show: false
    };
    this.topAnim = new Animated.Value(height);
    this.maskAnim = new Animated.Value(0);
  }

  componentDidMount() {}

  resetDate = () => {
    this.close();
    this.setState({
      selectedStartDate: null,
      selectedEndDate: null,
    });
  }

  onDateChange = (date, type) => {
    date = date.getTime();
    if (type === 'END_DATE') {
      this.setState({
        selectedEndDate: moment(date).startOf('day').toDate()
      });
    } else {
      this.setState({
        selectedStartDate: moment(date).toDate(),
        selectedEndDate: moment(date).startOf('day').toDate()
      });
    }
  };

  onConfirmClick = () => {
    this.close();
    this.props.onConfirmClick({
      selectedStartDate: this.state.selectedStartDate,
      selectedEndDate: this.state.selectedEndDate
    });
  };

  open = () => {
    const calendarHeight = Screen.height - (320 * (Screen.width / 375) + 65);
    this.setState({
      show: true
    });
    Animated.parallel([
      Animated.timing(this.maskAnim, {
        toValue: 1,
        duration: 600,
        easing: Easing.linear
      }),
      Animated.timing(this.topAnim, {
        toValue: -1 * (Screen.height / 2) + Screen.moderateScale((calendarHeight / 2) + 65),
        duration: 600,
        easing: Easing.linear
      })
    ]).start();
  };

  close = () => {
    Animated.parallel([
      Animated.timing(this.maskAnim, {
        toValue: 0,
        duration: 600,
        easing: Easing.linear
      }),
      Animated.timing(this.topAnim, {
        toValue: height,
        duration: 600,
        easing: Easing.linear
      })
    ]).start(() => {
      this.setState(
        {
          show: false
        },
        () => {
          this.props.onClose();
        }
      );
    });
  };

  renderSelectedRange = () => {
    const isRangeSelected =
      this.state.selectedStartDate != null &&
      this.state.selectedEndDate != null;

    if (isRangeSelected) {
      const selectedStart = moment(this.state.selectedStartDate);
      const selectedEnd = moment(this.state.selectedEndDate);
      return (
        <TouchableOpacity style={styles.preview} activeOpacity={1}>
          <View style={styles.dateStartBlock}>
            <Text style={styles.titleYearText}>
              {selectedStart.format('YYYY')}
            </Text>
            <Text style={styles.titleDateText}>
              {`${selectedStart.format('MM/DD')}(週${WEEKDAYS_ZH[selectedStart.format('e')]})`}
            </Text>
          </View>
          <Text style={styles.titleMiddleText}>至</Text>
          <View style={styles.dateStartBlock}>
            <Text style={styles.titleYearText}>
              {selectedEnd.format('YYYY')}
            </Text>
            <Text style={styles.titleDateText}>
              {`${selectedEnd.format('MM/DD')}(週${WEEKDAYS_ZH[selectedEnd.format('e')]})`}
            </Text>
          </View>
        </TouchableOpacity>
      );
    }
    return null;
  };

  generatePicker = () => {
    const calendarHeight = Screen.height - (320 * (Screen.width / 375) + 65);
    const topContent = Screen.height - ((Screen.height / 2) + (calendarHeight / 2) + 40);
    const bottomContent = Screen.height - calendarHeight * 2 + 60;
    if (this.state.show) {
      if (Platform.OS === 'ios') {
        return (
          <Animated.View
            style={[
              styles.mask,
              {
                opacity: this.maskAnim
              },
              this.props.hide ? {
 height: 0, width: 0, top: 0, left: 0
} : {}
            ]}
          >
            {/*<TouchableOpacity style={{ position: 'absolute', height: topContent, width, zIndex: 1}}
              activeOpacity={1}
              onPress={() => {
                if (this.state.show) {
                  //this.close();
                }
              }}
            />*/}
            <Animated.View
              ref={ref => this.calendarPickerView = ref}
              style={[
                styles.container,
                {
                  top: this.topAnim,
                  paddingBottom: this.props.paddingBottom,
                }
              ]}
            >
              {this.renderSelectedRange()}
              <CalendarPicker
                initialDate={this.state.selectedStartDate ? new Date(this.state.selectedStartDate) : new Date()}
                selectedStartDate={this.state.selectedStartDate}
                selectedEndDate={this.state.selectedEndDate}
                headerStyle={styles.headerStyle}
                headerTextStyle={styles.headerTextStyle}
                headerMidTextStyle={styles.headerMidTextStyle}
                headerSideTextStyle={styles.headerSideTextStyle}
                weekDaysStyle={styles.weekDaysStyle}
                weekDaysTextStyle={styles.weekDaysTextStyle}
                  //markedDays={['2017/5/10', '2017/5/1']}
                previousTitle="上月"
                nextTitle="下月"
                allowRangeSelection
                daySelectedStyle={styles.daySelectedStyle}
                selectedDayColor={colors.lightblue}
                selectedTextColor={colors.white}
                selectedMarkedDaysColorStyle={styles.calendarMarkedDaysColorStyle}
                selectedMarkedDaysTextColorStyle={{ color: colors.whiteThree }}
                months={MONTHS_ZH}
                weekdays={WEEKDAYS_ZH}
                onDateChange={this.onDateChange}
                inRangeTextStyle={{ color: colors.whiteThree }}
                calendarStyle={styles.calendarStyle}
              />
              <BottomButton
                style={{ position: 'relative' }}
                btnArray={[
                  {
                    text: '取 消',
                    textStyle: styles.bottomButtonText,
                    onPress: () => {
                      if (this.state.show) {
                        this.close();
                      }
                    }
                  },
                  {
                    text: '確 定',
                    textStyle: styles.bottomButtonText,
                    onPress: this.onConfirmClick
                  }
                ]}
              />
              {/*<TouchableOpacity style={{ position: 'absolute', bottom: Screen.moderateScale(-160), height: Screen.moderateScale(bottomContent), width, zIndex: 1}}
                  activeOpacity={1}
                  onPress={() => {
                    if (this.state.show) {
                      //this.close();
                    }
                  }}
                />*/}
            </Animated.View>
          </Animated.View>
        );
      }
      return (
        <View
          style={[
            styles.mask,
            {
              opacity: 1
            },
              //this.props.hide ? { height: 0, width: 0, top: 0, left: 0 } : {}
          ]}
        >
          {/*<TouchableOpacity style={{ position: 'absolute', height: topContent - 40, width, zIndex: 1}}
                activeOpacity={1}
                onPress={() => {
                  if (this.state.show) {
                    //this.close();
                  }
                }}
              />*/}
          <View
            style={[
              styles.container,
              {
                top: -1 * (Screen.height / 2) + Screen.moderateScale((calendarHeight / 2) + 65),
                paddingBottom: this.props.paddingBottom
              }
            ]}
          >
            {this.renderSelectedRange()}
            <CalendarPicker
              initialDate={this.state.selectedStartDate ? new Date(this.state.selectedStartDate) : new Date()}
              selectedStartDate={this.state.selectedStartDate}
              selectedEndDate={this.state.selectedEndDate}
              headerStyle={styles.headerStyle}
              headerTextStyle={styles.headerTextStyle}
              headerMidTextStyle={styles.headerMidTextStyle}
              headerSideTextStyle={styles.headerSideTextStyle}
              weekDaysStyle={styles.weekDaysStyle}
              weekDaysTextStyle={styles.weekDaysTextStyle}
                  //markedDays={['2017/5/10', '2017/5/1']}
              previousTitle="上月"
              nextTitle="下月"
              allowRangeSelection
              daySelectedStyle={styles.daySelectedStyle}
              selectedDayColor={colors.lightblue}
              selectedTextColor={colors.white}
              selectedMarkedDaysColorStyle={styles.calendarMarkedDaysColorStyle}
              selectedMarkedDaysTextColorStyle={{ color: colors.whiteThree }}
              months={MONTHS_ZH}
              weekdays={WEEKDAYS_ZH}
              onDateChange={this.onDateChange}
              inRangeTextStyle={{ color: colors.whiteThree }}
              calendarStyle={styles.calendarStyle}
            />
            <BottomButton
              style={{ position: 'relative' }}
              btnArray={[
                {
                  text: '取 消',
                  textStyle: styles.bottomButtonText,
                  onPress: () => {
                    if (this.state.show) {
                      this.close();
                    }
                  }
                },
                {
                  text: '確 定',
                  textStyle: styles.bottomButtonText,
                  onPress: this.onConfirmClick
                }
              ]}
            />
            {/*<TouchableOpacity style={{ position: 'absolute', bottom: -210, height: bottomContent, width, zIndex: 1}}
                  activeOpacity={1}
                  onPress={() => {
                    if (this.state.show) {
                      //this.close();
                    }
                  }}
                />*/}
          </View>
        </View>
      );
    }
    return <View />;
  };

  render() {
    return this.generatePicker();
  }
}
