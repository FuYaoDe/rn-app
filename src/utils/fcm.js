import { Platform, Alert } from 'react-native';
import FCM, {
  FCMEvent,
} from 'react-native-fcm';
import handleNotify from './handleNotify';

const fcm = {
  getToken: async () => {
    const token = await FCM.getFCMToken();
    console.log('fcm token', token);
    return token;
  },
  init: ({ updateAlert, updateNotifyAction }) => {
    console.log('FCM init');
    if (Platform.OS === 'ios') {
      FCM.requestPermissions();
    }

    FCM.on(FCMEvent.Notification, async (notify) => {
      // there are two parts of notif. notif.notification contains the notification payload, notif.data contains data payload
      if (notify.local_notification) {
        //this is a local notification
      }
      if (notify.opened_from_tray) {
        //app is open/resumed because user clicked banner
      }
      handleNotify.get({ notify, updateAlert, updateNotifyAction });
    });


  },
  reopen: ({ updateAlert, updateNotifyAction }) => {
    FCM.getInitialNotification().then((notify) => {

      if (Platform.OS === 'ios' && notify) {
        notify.opened_from_tray = 1;
        handleNotify.get({ notify, updateAlert, updateNotifyAction });
      } else if (notify.opened_from_tray && notify.notification) {
        handleNotify.get({ notify, updateAlert, updateNotifyAction });
      }
    });
  },
};
export default fcm;
