import { Platform, Alert } from 'react-native';
import _ from 'lodash';
import timeoutFetch from 'react-native-fetch-polyfill';
import { Actions } from 'react-native-router-flux';
import config from '../config';
import { api, apiAction as apiActionConfig } from '../config/api';
import Storage from '../config/storage';
import { getItem } from './asyncStorage';
import i18n, { i18nKey } from '../utils/i18n';


function FileSizeError(message) {
  this.name = 'FileSizeError';
  this.message = (message || '');
}
FileSizeError.prototype = Error.prototype;

const serialize = (obj, prefix) => {
  const str = [];
  for (const p in obj) {
    if (obj.hasOwnProperty(p)) {
      let k = prefix ? `${prefix}[${p}]` : p,
        v = obj[p];
      str.push(typeof v === 'object' ?
        serialize(v, k) :
        `${encodeURIComponent(k)}=${encodeURIComponent(v)}`);
    }
  }
  return str.join('&');
};

export const apiAction = apiActionConfig;

let apiHandlerQueued = {};
export const apiHandler = async ({
  res, done = () => {}, fail = () => {}, notify = true, errorTitle
}) => {
  if (res.success) {
    if (notify) {
      if (res.user_title && res.user_msg) {
        Alert.alert(res.user_title, res.user_msg, [{ text: i18n.t(i18nKey.apiAlertAction), onPress: () => {} }]);
      } else if (res.user_msg) {
        Alert.alert(i18n.t(i18nKey.apiAlertSuccessTitle), res.user_msg, [{ text: i18n.t(i18nKey.apiAlertAction), onPress: () => {} }]);
      } else if (res.message) {
        // Alert.alert('成功', res.msg);
      }
    }
    if (res.reFetch) {
      res.reFetch = false;
      res.queued = false;
      await apiHandler({
        res,
        ...apiHandlerQueued[res.queueId],
      });
      console.log(`${res.queueId} ReFetch done.`);
      delete apiHandlerQueued[res.queueId];
    } else if (res.queued) {
      apiHandlerQueued = {
        ...apiHandlerQueued,
        [res.queueId]: { done, fail, alert },
      };
    } else {
      await done(res);
    }
  } else if (res.result === -9999) {
    console.log('already alert -99');
  } else {
    if (res.responseStatus === 401 || res.responseStatus === 403) {
      Actions.systemLogout({
        message: res.error_user_msg || res.msg || res.message,
      });
    } else if (notify) {
      let errorInfo = '';
      if (res.errors && _.isArray(res.errors)) {
        res.errors.forEach((data) => {
          errorInfo += `${data.error_user_msg}\n`;
        });
      }
      if (res.error_user_msg && res.error_user_title) {
        Alert.alert(res.error_user_title, `${res.error_user_msg}\n${errorInfo}`, [{ text: i18n.t(i18nKey.apiAlertAction), onPress: () => {} }]);
      } else if (res.error_user_msg) {
        Alert.alert(errorTitle || i18n.t(i18nKey.apiAlertTitle), `${res.error_user_msg}\n${errorInfo}`, [{ text: i18n.t(i18nKey.apiAlertAction), onPress: () => {} }]);
      } else if (res.message) {
        Alert.alert(errorTitle || i18n.t(i18nKey.apiAlertTitle), `${res.message}`, [{ text: i18n.t(i18nKey.apiAlertAction), onPress: () => {} }]);
      } else {
        Alert.alert(errorTitle || i18n.t(i18nKey.apiAlertTitle), '請稍後再試', [{ text: i18n.t(i18nKey.apiAlertAction), onPress: () => {} }]);
      }
    }
    await fail(res);
  }
};

export const apifetch = async (action, data = {}, options = {}) => {
  let url = config.domain + api[action].url;

  const method = api[action].method.toUpperCase();
  const body = {
    appVersion: `${Platform.OS}  ${config.version}`,
    ...data,
    ...api[action].data,
    langCode: i18n.getDeviceLocale(),
    // type: data.type || 'A',
  };
  const token = await getItem(Storage.AUTHORIZATIO);

  const requestOption = {
    method,
    headers: {
      Accept: 'application/json',
      'Accept-Language': i18n.getDeviceLocale(),
    },
    timeout: config.timeout,
    ...options,
  };

  const auth = api[action].auth;
  if (auth) {
    if (_.isEmpty(token)) {
      return {
        result: -9999,
      };
    }
    requestOption.headers.Authorization = `Bearer ${token}`;
  }
  if (!_.isEmpty(body)) {
    if (method === 'GET') {
      url += `?${serialize(body)}`;
    } else {
      /* formData */
      // const formData = new FormData();
      // for(const name in body) {
      //   if (name && !_.isEmpty(body[name]) || _.isNumber(body[name])) {
      //     console.log(name, body[name]);
      //     if (_.isArray(body[name])) {
      //       formData.append(name, `[${body[name]}]`);
      //     } else {
      //       formData.append(name, body[name]);
      //     }
      //   }
      // }

      /* JSON */
      requestOption.body = JSON.stringify(body);
    }
  }
  let responseJson;
  try {
    const response = await timeoutFetch(url, requestOption);
    responseJson = await response.json();
    responseJson.responseStatus = response.status;

    return responseJson;
  } catch (error) {
    let msg = '';
    switch (error.name) {
      case 'FileSizeError':
        msg = error.message;
        break;
      default:
        msg = '請稍後再試';
        break;
    }
    return {
      result: -1,
      error_code: 87,
      msg,
    };
  }
  return {};
};
