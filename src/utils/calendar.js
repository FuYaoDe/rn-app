export function onDateChange(thisProps, nextProps, cb) {
  const thisSelectedStartDate = thisProps.calendar.selectedStartDate;
  const nextSelectedStartDate = nextProps.calendar.selectedStartDate;

  const thisSelectedEndDate = thisProps.calendar.selectedEndDate;
  const nextSelectedEndDate = nextProps.calendar.selectedEndDate;

  if (thisSelectedStartDate !== nextSelectedStartDate || thisSelectedEndDate !== nextSelectedEndDate) {
    cb();
  }
}

