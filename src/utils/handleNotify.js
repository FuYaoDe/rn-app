import { Platform, Alert } from 'react-native';
import { Actions } from 'react-native-router-flux';
import FCM, {
  FCMEvent,
  RemoteNotificationResult,
  WillPresentNotificationResult,
  NotificationType,
} from 'react-native-fcm';


const handleNotify = {
  get: ({ notify, updateAlert, updateNotifyAction }) => {
    const notifyData = handleNotify.format(notify);
    if (notify.opened_from_tray) {
      setTimeout(() => {
        handleNotify.action({
          type: notifyData.type,
          title: '測試',
          service: notifyData.service,
          updateNotifyAction,
          actionSubject: notifyData.actionSubject,
          actionTime: notifyData.actionTime,
          message: notifyData.message,
        });
      }, 800);
    } else {
      updateAlert({
        title: 'GEC',
        desc: notifyData.message,
        status: 'show',
        type: notifyData.type,
        service: notifyData.service,
        actionSubject: notifyData.actionSubject,
        actionTime: notifyData.actionTime,
      });
    }
  },
  format: (notify) => {
    const result = {};
    if (Platform.OS === 'ios') {
      result.message = notify.aps.alert;
      result.type = JSON.parse(notify.actionType).StringValue;
      result.service = notify.service ? JSON.parse(notify.service).StringValue : '';
      result.actionSubject = notify.actionSubject ? JSON.parse(notify.actionSubject).StringValue : '';
      result.actionTime = notify.actionTime ? JSON.parse(notify.actionTime).StringValue : '';
    } else {
      console.log('!!!!!', notify);
      if (notify.opened_from_tray) {
        // 在背景收到通知，點起 APP
        result.message = notify.notification.message;
        result.type = JSON.parse(notify.notification.actionType).StringValue;
        result.service = notify.service ? JSON.parse(notify.notification.service).StringValue : '';
        result.actionSubject = notify.notification.actionSubject ? JSON.parse(notify.notification.actionSubject).StringValue : '';
        result.actionTime = notify.notification.actionTime ? JSON.parse(notify.notification.actionTime).StringValue : '';
      } else {
        // 在前景收到通知
        result.message = notify.message;
        result.type = JSON.parse(notify.actionType).StringValue;
        result.service = notify.service ? JSON.parse(notify.service).StringValue : '';
        result.actionSubject = notify.actionSubject ? JSON.parse(notify.actionSubject).StringValue : '';
        result.actionTime = notify.actionTime ? JSON.parse(notify.actionTime).StringValue : '';
      }
    }
    return result;
  },
  action: ({
    updateNotifyAction, type, title, service, actionSubject, actionTime, message
  }) => {
    updateNotifyAction({ isAction: false, type, service });
    if (type === 'pending') {
      Actions.tracking({ initialPage: 0 });
    } else if (type === 'processing') {
      Actions.tracking({ initialPage: 1 });
    } else if (type === 'finish') {
      Actions.tracking({ initialPage: 2 });
    } else if (type === 'members') {
      Actions.notifyDetail({
        subject: actionSubject,
        message,
        time: actionTime,
      });
    }
  }
};
export default handleNotify;
