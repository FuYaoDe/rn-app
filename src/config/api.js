export const apiAction = {

  LOGIN: 'LOGIN',
  LOGOUT: 'LOGOUT',
};

export const api = {
  [apiAction.LOGIN]: {
    url: '/api/auth/local',
    method: 'post',
    auth: false,
    data: {}
  },
  [apiAction.LOGOUT]: {
    url: '/logout',
    method: 'post',
    auth: true,
    data: {}
  },
};
