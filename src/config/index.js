import { Platform } from 'react-native';

const envMode = 'development';
let config = {};
const domain = Platform.OS === 'ios' ? '127.0.0.1' : '192.168.2.3';

const defaultConfig = {
  ...Platform.select({
    ios: {
      version: '1.0.0'
    },
    android: {
      version: '1.0.0'
    }
  }),
  envMode,
  aes: {
    key: '1234567812345678',
    iv: '1234567812345678'
  },
  openFetchLog: false,
  disableYellowBox: true,
  timeout: 30 * 1000
};

// --------------- dev mode -------------
if (envMode === 'development') {
  config = {
    ...defaultConfig,
    // initPage: 'auth',
    // domain: 'https://beta.labfnp.com',
    // domain: "http://192.168.88.12:5002",
    // domain: `http://${domain}:5001`
    // domain: 'https://gec-909b4a5e.trunksys.com',
    domain: 'https://admin.crm-mediglobal.org',
  };
} else {
  config = {
    ...defaultConfig
    // domain: 'https://hcdev.digihome.com.tw'
  };
}

export default config;
