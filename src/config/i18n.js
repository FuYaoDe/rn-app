export const i18nKey = {
  loginIdInput: 'loginIdInput',
  loginPasswordInput: 'loginPasswordInput',
  loginForgetpassword: 'loginForgetpassword',
  loginErrorMsg: 'loginErrorMsg',
  loginButton: 'loginButton',
  loginPrivacyMsg: 'loginPrivacyMsg',
  loginPrivacyPolicy: 'loginPrivacyPolicy',
  loginUserTerms: 'loginUserTerms',
  loginAnd: 'loginAnd',
  drawerHome: 'drawerHome',
  drawerNotify: 'drawerNotify',
  drawerCEU: 'drawerCEU',
  drawerProfile: 'drawerProfile',
  drawerFriend: 'drawerFriend',
  drawerUserTerms: 'drawerUserTerms',
  drawerPrivacy: 'drawerPrivacy',
  drawerLogout: 'drawerLogout',

  logoutTitle: 'logoutTitle',
  logoutMsg: 'logoutMsg',
  logoutConfirm: 'logoutConfirm',
  logoutCancel: 'logoutCancel',

  networkFail: 'networkFail',
  networkFailMsg: 'networkFailMsg',
  networkFailRetry: 'networkFailRetry',

  apiAlertTitle: 'apiAlertTitle',
  apiAlertSuccessTitle: 'apiAlertSuccessTitle',
  apiAlertAction: 'apiAlertAction',

  systemLogoutTitle: 'systemLogoutTitle',
  systemLogoutAction: 'systemLogoutAction'
};

export default {
  loginIdInput: {
    'zh-TW': '帳戶 ID',
    en: 'User ID',
  },
  loginPasswordInput: {
    'zh-TW': '密碼',
    en: 'Password',
  },
  loginForgetpassword: {
    'zh-TW': '忘記密碼',
    en: 'Forgot password',
  },
  loginErrorMsg: {
    'zh-TW': '帳戶 ID 或密碼輸入錯誤',
    en: 'Invalid user ID or password',
  },
  loginButton: {
    'zh-TW': '登入',
    en: 'Sign In',
  },
  loginPrivacyMsg: {
    'zh-TW': '我已詳細閱讀且同意',
    en: 'I agree to the',
  },
  loginPrivacyPolicy: {
    'zh-TW': '隱私權條款',
    en: 'Privacy Policy',
  },
  loginUserTerms: {
    'zh-TW': '使用者條款',
    en: 'Terms of Use',
  },
  loginAnd: {
    'zh-TW': '和',
    en: 'and',
  },
  drawerHome: {
    'zh-TW': '首頁',
    en: 'Home',
  },
  drawerNotify: {
    'zh-TW': '通知',
    en: 'Notification',
  },
  drawerCEU: {
    'zh-TW': 'CEU通訊',
    en: 'CEU Newsletter',
  },
  drawerProfile: {
    'zh-TW': '個人資訊',
    en: 'My Profile',
  },
  drawerFriend: {
    'zh-TW': '推薦好友',
    en: 'Recommend a Friend',
  },
  drawerUserTerms: {
    'zh-TW': '使用者條款',
    en: 'Terms of Use',
  },
  drawerPrivacy: {
    'zh-TW': '隱私權條款',
    en: 'Privacy Policy',
  },
  drawerLogout: {
    'zh-TW': '登出',
    en: 'Sign Out',
  },


  applicationApply: {
    'zh-TW': '申請',
    en: 'Apply',
  },
  applicationProceed: {
    'zh-TW': '申請確認',
    en: 'Proceed to application',
  },
  applicationProceedMsg: {
    'zh-TW': '我已詳細閱讀內文及相關規章，並瞭解且同意其條款。',
    en: 'I have read and fully understand the contents. I agree to the terms and conditions.',
  },
  applicationProceedConfirm: {
    'zh-TW': '我同意',
    en: 'Confirm',
  },
  applicationProceedCancel: {
    'zh-TW': '取消',
    en: 'Cancel',
  },
  applicationPageTitle: {
    'zh-TW': '您想要申請的項目？',
    en: 'What would you like to request?',
  },
  applicationNotes: {
    'zh-TW': '請輸入備註',
    en: 'Enter notes',
  },
  applicationSubmit: {
    'zh-TW': '提交申請',
    en: 'Submit',
  },
  applicationSuccess: {
    'zh-TW': '申請完成',
    en: 'Your request has been submitted.',
  },
  applicationSuccessConfirm: {
    'zh-TW': '確定',
    en: 'Confirm',
  },
  applicationCall: {
    'zh-TW': '通話',
    en: 'Call',
  },
  applicationCallCancel: {
    'zh-TW': '取消',
    en: 'Cancel',
  },

  trackingAction: {
    'zh-TW': '申請進度',
    en: 'Tracking',
  },
  trackingUnattended: {
    'zh-TW': '未處理',
    en: 'Unattended',
  },
  trackingInProgress: {
    'zh-TW': '處理中',
    en: 'In Progress',
  },
  trackingCompleted: {
    'zh-TW': '已完成',
    en: 'Completed',
  },
  trackingRequiredDocuments: {
    'zh-TW': '所需文件',
    en: 'Required Documents',
  },
  trackingPending: {
    'zh-TW': '待收件',
    en: 'Pending',
  },
  trackingCaseFileNo: {
    'zh-TW': '案件單號：',
    en: 'Case ID:',
  },
  trackingNoApplication: {
    'zh-TW': '尚無資料',
    en: 'No Data',
  },
  notifyTrackingNotes: {
    'zh-TW': '待辦通知',
    en: 'Tracking Notes',
  },
  notifyNews: {
    'zh-TW': '最新消息',
    en: 'News',
  },
  notifyNoMessage: {
    'zh-TW': '尚無新消息',
    en: 'No News',
  },
  profileTitle: {
    'zh-TW': '請洽服務人員',
    en: 'Contact us',
  },
  profileMsg: {
    'zh-TW': '如需修改資料，請 Email 至：customerservice@mediglobal.org',
    en: 'Inform us the changes by sending Email：customerservice@mediglobal.org',
  },
  profileListMsg: {
    'zh-TW': '如需修改資料，請按 [ 聯絡我們 ] 進行資料修正。',
    en: 'If your information is incorrect, please hit [ Contact Us ] to inform us the changes.',
  },
  profileName: {
    'zh-TW': '姓名',
    en: 'Name',
  },
  profileEmail: {
    'zh-TW': '電子信箱',
    en: 'Email',
  },
  profilePassword: {
    'zh-TW': '密碼',
    en: 'Password',
  },
  profilePasswordUpdateSuccess: {
    'zh-TW': '儲存成功',
    en: 'saved',
  },
  profilePhone: {
    'zh-TW': '原通訊手機',
    en: 'Phone no.',
  },
  profileHomeAddress: {
    'zh-TW': '主要聯絡/郵寄地址',
    en: 'Home address',
  },
  profileCurrentPhone: {
    'zh-TW': '留學地手機',
    en: 'Current phone no.',
  },
  profileCurrentAddress: {
    'zh-TW': '留學地住址',
    en: 'Current address',
  },
  profilePassportNo: {
    'zh-TW': '護照號碼',
    en: 'Passport no. ',
  },
  profilePassportExpiryDate: {
    'zh-TW': '護照到期日',
    en: 'Passport expiry date',
  },
  profileParentPhoneNo: {
    'zh-TW': '電話號碼',
    en: 'Phone no.',
  },
  profileMailingAddress: {
    'zh-TW': '通訊地址',
    en: 'Mailing address',
  },
  profileContactUs: {
    'zh-TW': '聯絡我們',
    en: 'Contact us',
  },

  friendMsg: {
    'zh-TW': '若您有親友有興趣至西班牙攻讀醫牙科系，請留下聯絡方式，我們將盡速聯繫！',
    en: 'If any of your friend is interested in studying dentistry or medicine in Spain, please leave us his/her information, we will contact them in the near future.',
  },
  friendName: {
    'zh-TW': '親友姓名',
    en: "Your Friend's Name",
  },
  friendTitle: {
    'zh-TW': '稱謂',
    en: 'Title',
  },
  friendOccupation: {
    'zh-TW': '職業',
    en: 'Occupation',
  },
  friendPhoneNumber: {
    'zh-TW': '聯絡電話',
    en: 'Phone Number',
  },
  friendMr: {
    'zh-TW': '先生',
    en: 'Mr.',
  },
  friendMrs: {
    'zh-TW': '女士',
    en: 'Mrs.',
  },
  friendSubmit: {
    'zh-TW': '送出',
    en: 'Submit',
  },
  friendSuccessTitle: {
    'zh-TW': '推薦成功',
    en: ' ',
  },
  friendSuccessMsg: {
    'zh-TW': '感謝您的推薦',
    en: 'Thank you for your recommendation!',
  },
  friendConfirm: {
    'zh-TW': '確定',
    en: 'Confirm',
  },

  logoutTitle: {
    'zh-TW': '登出',
    en: 'Sign Out',
  },
  logoutMsg: {
    'zh-TW': '您將會被登出。',
    en: 'You are about to sign out.',
  },
  logoutConfirm: {
    'zh-TW': '登出',
    en: 'Sign Out',
  },
  logoutCancel: {
    'zh-TW': '取消',
    en: 'Cancel',
  },

  // forgetPasswordTitle: 'forgetPasswordTitle',
  forgetPasswordTitle: {
    'zh-TW': '忘記密碼?',
    en: 'Forgot password?',
  },
  // forgetPasswordIDEmailMsg: 'forgetPasswordIDEmailMsg',
  forgetPasswordIDEmailMsg: {
    'zh-TW': '請輸入您的帳戶 ID 以及電子郵件',
    en: 'Please enter your user ID and email.',
  },
  // forgetPasswordEmailInput: 'forgetPasswordEmailInput',
  forgetPasswordEmailInput: {
    'zh-TW': '電子郵件',
    en: 'Email',
  },
  // forgetPasswordIdInput: 'forgetPasswordIdInput',
  forgetPasswordIdInput: {
    'zh-TW': '帳戶 ID',
    en: 'ID',
  },
  // forgetPasswordIDEmailErrorMsg: 'forgetPasswordIDEmailErrorMsg',
  forgetPasswordIDEmailErrorMsg: {
    'zh-TW': 'Email 或 帳戶 ID 不符',
    en: 'Invalid user ID or password',
  },
  // forgetPasswordNext: 'forgetPasswordNext',
  forgetPasswordNext: {
    'zh-TW': '下一步',
    en: 'Next',
  },
  // forgetPasswordEnterCodeMsg: 'forgetPasswordEnterCodeMsg',
  forgetPasswordEnterCodeMsg: {
    'zh-TW': '請輸入驗證碼',
    en: 'Please enter the verification code.',
  },
  // forgetPasswordSendCodeMsg: 'forgetPasswordSendCodeMsg',
  forgetPasswordSendCodeMsg: {
    'zh-TW': '驗證碼會寄到您的電子郵件中',
    en: 'The verification code will be sent to your email.',
  },
  // forgetPasswordEnterCodeIn: 'forgetPasswordEnterCodeIn',
  forgetPasswordEnterCodeIn: {
    'zh-TW': '請於',
    en: 'Enter verification code in',
  },
  // forgetPasswordEnter: 'forgetPasswordEnter',
  forgetPasswordEnter: {
    'zh-TW': '秒內輸入驗證碼',
    en: 'seconds.',
  },

  // forgetPasswordCancel: 'forgetPasswordCancel',
  forgetPasswordCancel: {
    'zh-TW': '取消',
    en: 'Cancel',
  },
  // forgetPasswordOK: 'forgetPasswordOK',
  forgetPasswordOK: {
    'zh-TW': '確定',
    en: 'OK',
  },
  // forgetPasswordResetPassword: 'forgetPasswordResetPassword',
  forgetPasswordResetPassword: {
    'zh-TW': '變更密碼',
    en: 'Reset password',
  },
  // forgetPasswordEnterNewPassword: 'forgetPasswordEnterNewPassword',
  forgetPasswordEnterNewPassword: {
    'zh-TW': '請輸入新密碼',
    en: 'Enter new password',
  },
  // forgetPasswordReEnterPassword: 'forgetPasswordReEnterPassword',
  forgetPasswordReEnterPassword: {
    'zh-TW': '重新輸入新密碼',
    en: 'Re-enter new password',
  },
  // forgetPasswordSavePassword: 'forgetPasswordSavePassword',
  forgetPasswordSavePassword: {
    'zh-TW': '儲存',
    en: 'Save',
  },
  forgetPasswordSavePassword: {
    'zh-TW': '儲存',
    en: 'Save',
  },
  // forgetPasswordInvalidPassword: 'forgetPasswordInvalidPassword',
  forgetPasswordInvalidPassword: {
    'zh-TW': '密碼不符',
    en: 'Invalid password',
  },
  // forgetPasswordTryAgain: 'forgetPasswordTryAgain',
  forgetPasswordInvalidPassword: {
    'zh-TW': '密碼不符',
    en: 'Invalid password',
  },
  // forgetPasswordInvalidVerificationCode: 'forgetPasswordInvalidVerificationCode',
  forgetPasswordInvalidVerificationCode: {
    'zh-TW': '驗證碼不正確',
    en: 'Invalid verification code',
  },
  // forgetPasswordPleaseTryAgain: 'forgetPasswordPleaseTryAgain',
  forgetPasswordPleaseTryAgain: {
    'zh-TW': '請重試',
    en: 'Please try again.',
  },
  // forgetPasswordResend: 'forgetPasswordResend',
  forgetPasswordResend: {
    'zh-TW': '重新發送',
    en: 'Resend',
  },
  // forgetPasswordResendVerificationCode: 'forgetPasswordResendVerificationCode',
  forgetPasswordResendVerificationCode: {
    'zh-TW': '重發驗證碼',
    en: 'Resend verification code',
  },

  networkFail: {
    'zh-TW': '網路異常',
    en: 'Unable to Connect',
  },
  networkFailMsg: {
    'zh-TW': '網路無法連線請連線後再試',
    en: 'Please check your Internet connection and try again.',
  },
  networkFailRetry: {
    'zh-TW': '重新整理',
    en: 'Refresh',
  },


  // updatePasswordTitle: 'updatePasswordTitle',
  updatePasswordTitle: {
    'zh-TW': '變更密碼',
    en: 'Reset password',
  },
  // updatePasswordCurrentPassword: 'updatePasswordCurrentPassword',
  updatePasswordCurrentPassword: {
    'zh-TW': '請輸入舊密碼',
    en: 'Enter current password',
  },
  // updatePasswordNewPassword: 'updatePasswordNewPassword',
  updatePasswordNewPassword: {
    'zh-TW': '請輸入新密碼',
    en: 'Enter new password',
  },
  // updatePasswordReEnterNewPassword: 'updatePasswordReEnterNewPassword',
  updatePasswordReEnterNewPassword: {
    'zh-TW': '重新輸入新密碼',
    en: 'Re-enter new password',
  },
  // updatePasswordSave: 'updatePasswordSave',
  updatePasswordSave: {
    'zh-TW': '儲存',
    en: 'Save',
  },
  // updatePasswordAlertTitle: 'updatePasswordAlertTitle',
  updatePasswordAlertTitle: {
    'zh-TW': '警告',
    en: 'Attention',
  },
  updatePasswordInvalidPassword: {
    'zh-TW': '密碼不符',
    en: 'Invalid password',
  },
  // updatePasswordRetry: 'updatePasswordRetry',
  updatePasswordRetry: {
    'zh-TW': '重試',
    en: 'Retry',
  },

  apiAlertTitle: {
    'zh-TW': '警告',
    en: 'Attention',
  },

  apiAlertSuccessTitle: {
    'zh-TW': '成功',
    en: 'Success',
  },

  apiAlertAction: {
    'zh-TW': '確定',
    en: 'Confirm',
  },

  // systemLogoutTitle: 'systemLogoutTitle',
  // systemLogoutAction: 'systemLogoutAction',
  systemLogoutTitle: {
    'zh-TW': '警告',
    en: 'Attention',
  },
  systemLogoutAction: {
    'zh-TW': '登入',
    en: 'Login',
  }
};
