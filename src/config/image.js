export default {
  logo: require('../assets/gec/logo.png'),
  drawerLogo: require('../assets/gec/logo/logo.png'),
  big_logo: require('../assets/gec/big_logo.png'),
  menu: require('../assets/gec/icon/menu.png'),
  menu: require('../assets/gec/icon/menu.png'),

  call: require('../assets/gec/icons/services/call.png'),
  location: require('../assets/gec/icons/services/location.png'),

  bk_lightgray: require('../assets/gec/bk/lightgray.png'),

  phone: require('../assets/gec/icon/phonecall.png'),
  gps: require('../assets/gec/icon/servicelocation.png'),

  dropdownIcon: require('../assets/gec/icon/dropdown.png'),

};
