import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Text, View, ViewPropTypes, TouchableOpacity, Alert, Image, Platform } from 'react-native';
import { Actions, ActionConst } from 'react-native-router-flux';
import { bindActionCreators } from 'redux';
import { connect, } from 'react-redux';
import Storage from '../config/storage';
import Screen from '../utils/screen';
import { LineBtn } from '../widget/Button';
import { H3 } from '../widget/Label';
import NotifyBox from '../components/NotifyBox';
import colors from '../config/color';
import images from '../config/image';
import config from '../config/index';
import { removeItem } from '../utils/asyncStorage';
import { cleanUser } from '../actions/user';
import { updateLoading } from '../actions/loading';
import { H4 } from '../widget/Label';
import i18n, { i18nKey } from '../utils/i18n';
import { apifetch, apiHandler, apiAction } from '../utils/api';
import fcm from '../utils/fcm';

const styles = StyleSheet.create({
  container: {
    zIndex: 100,
    flex: 1,
    // backgroundColor: 'rgba(0, 0, 0, 0.1)',
    justifyContent: 'center',
    paddingLeft: Screen.moderateScale(15),
  },
  item: {
    paddingTop: Screen.moderateScale(11),
    paddingBottom: Screen.moderateScale(11),
    fontWeight: '600',
  },
});

@connect(
  state => ({
    routes: state.routes,
  }),
  dispatch => bindActionCreators({
    cleanUser,
    updateLoading
  }, dispatch)
)
class DrawerContent extends React.Component {
  static propTypes = {
    name: PropTypes.string,
    sceneStyle: ViewPropTypes.style,
    title: PropTypes.string,
  }

  static contextTypes = {
    drawer: PropTypes.object,
  }

  logout = async () => {
    Alert.alert(i18n.t(i18nKey.logoutTitle), i18n.t(i18nKey.logoutMsg), [
      { text: i18n.t(i18nKey.logoutCancel), onPress: () => {} },
      {
        text: i18n.t(i18nKey.logoutConfirm),
        onPress: async () => {
          await removeItem(Storage.AUTHORIZATIO);
          Actions.auth({ type: ActionConst.RESET });
          this.props.cleanUser();
          // try {
          //   this.props.updateLoading(true);
          //   const res = await apifetch(apiAction.LOGOUT, {
          //     platform: Platform.OS === 'ios' ? 'IOS_FCM' : 'ANDROID',
          //     deviceToken: await fcm.getToken(),
          //   });
          //   await apiHandler({
          //     res,
          //     done: async(res) => {
          //       await removeItem(Storage.AUTHORIZATIO);
          //       Actions.auth({ type: ActionConst.RESET });
          //       this.props.cleanUser();
          //     //   this.props.resetServices();
          //     //   this.props.cleanTracking();
          //     //   this.props.updateLoading(false);
          //     },
          //     fail: () => {
          //       this.props.updateLoading(false);
          //     }
          //   });
          // } catch (error) {
          //   console.log(error);
          // }
        }
      }
    ], { cancelable: false });
  }

  render() {
    console.log('this.props.routes.scene.drawer ', this.props.routes.scene.drawer);
    if (this.props.routes.scene.drawer == 'DrawerClose') {
      return null;
    }
    return (
      <View style={styles.container}>
        <View style={{ flex: 87, justifyContent: 'center', }}>
          <TouchableOpacity onPress={Actions.posHome}>
            <H3 style={styles.item}>{i18n.t(i18nKey.drawerHome)}</H3>
          </TouchableOpacity>
          <TouchableOpacity onPress={Actions.sandbox}>
            <H3 style={styles.item}>Sandbox</H3>
          </TouchableOpacity>
          <TouchableOpacity onPress={this.logout}>
            <H3 style={styles.item}>登出</H3>
          </TouchableOpacity>
          {/* <TouchableOpacity style={{ flexDirection: 'row' }} onPress={Actions.notify}>
              <H3 style={styles.item}>{i18n.t(i18nKey.drawerNotify)}</H3>
              <NotifyBox
                amount={0}
                color={colors.pink}
                animated={'pop'}
                top={Screen.moderateScale(2)}
                left={0}
                max={99}
                small
              />
            </TouchableOpacity>
            <TouchableOpacity onPress={Actions.ceu}>
              <H3 style={styles.item}>{i18n.t(i18nKey.drawerCEU)}</H3>
            </TouchableOpacity>
            <TouchableOpacity onPress={Actions.profile}>
              <H3 style={styles.item}>{i18n.t(i18nKey.drawerProfile)}</H3>
            </TouchableOpacity>
            <TouchableOpacity onPress={Actions.friend}>
              <H3 style={styles.item}>{i18n.t(i18nKey.drawerFriend)}</H3>
            </TouchableOpacity>
            <TouchableOpacity onPress={Actions.userTerms}>
              <H3 style={styles.item}>{i18n.t(i18nKey.drawerUserTerms)}</H3>
            </TouchableOpacity>
            <TouchableOpacity onPress={Actions.privacy}>
              <H3 style={styles.item}>{i18n.t(i18nKey.drawerPrivacy)}</H3>
            </TouchableOpacity>
            <TouchableOpacity onPress={this.logout}>
              <H3 style={styles.item}>{i18n.t(i18nKey.drawerLogout)}</H3>
            </TouchableOpacity> */}
        </View>
        <View style={{
          flex: 20, paddingLeft: 20, flexDirection: 'row', alignItems: 'center'
        }}
        >
          {/* <Image source={images.drawerLogo} style={{ height: 48, width: 50 }} /> */}
          <View style={{ justifyContent: 'center', alignItems: 'center', paddingLeft: 12, }}>
            <H4 style={{ color: colors.pink }}>Version {config.version}</H4>
          </View>
        </View>
      </View >
    );
  }
}

export default DrawerContent;
