import React, { Component, PropTypes } from 'react';
import { connect, } from 'react-redux';
import { StyleSheet, Platform, Alert, Text, BackAndroid, View, ActivityIndicator, TouchableOpacity, NetInfo } from 'react-native';
import { Router, Scene, ActionConst, Actions, Drawer, Lightbox, Stack } from 'react-native-router-flux';
import _ from 'lodash';
import { bindActionCreators } from 'redux';
import moment from 'moment';
import DropdownAlert from 'react-native-dropdownalert';
import Entypo from 'react-native-vector-icons/Entypo';
import codePush from 'react-native-code-push';
import handleNotify from '../utils/handleNotify';
import { updateLoading } from '../actions/loading';
import { updateCalendar } from '../actions/calendar';
import { updateAlert } from '../actions/alert';
import { updateNotifyAction } from '../actions/notifyAction';
import Config from '../config';
import colors from '../config/color';
import Storage from '../config/storage';
import { getItem } from '../utils/asyncStorage';
import CustomNavBar from '../components/CustomNavBar';
import DrawerContent from '../containers/DrawerContent';
import { H3 } from '../widget/Label';
import CustomRouter from './CustomRouter';
import Login from './auth/Login';
import Home from './home/Home';
import SystemLogout from './SystemLogout';
import NetworkFail from './NetworkFail';
import TabIcon from '../components/TabIcon';
import SandBox from './SandBox/Index';
import ImagePick from './SandBox/ImagePick';
import PrivateTextPlace from './SandBox/PrivateTextPlace';
import Calendar from './SandBox/Calendar';
import InputBoxPlace from './SandBox/InputBoxPlace';
import FCMPlace from './SandBox/FCMPlace';
import AnimateDetail from './SandBox/AnimateDetail';
import AnimateList from './SandBox/AnimateList';
import TabView from './SandBox/TabView';
import TimePick from './SandBox/TimePick';
import Intro from './Intro';
import fcm from '../utils/fcm';
import Screen from '../utils/screen';
import CalendarPicker from '../widget/CalendarPicker';
import Gec from './SandBox/Gec';
import Gec2 from './SandBox/Gec2';
import Gec3 from './SandBox/Gec3';
import CardStackStyleInterpolator from 'react-navigation/src/views/CardStack/CardStackStyleInterpolator';


// const RouterWithRedux = connect()(Router);
// const RouterWithRedux = connect((state) => ({ state: state.routes }))(Router);

const styles = StyleSheet.create({
  loadingContent: {
    position: 'absolute',
    width: Screen.width,
    height: Screen.height,
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 1,
  },
  loadingCover: {
    width: 100,
    height: 100,
    backgroundColor: 'rgba(100,100,100,0.5)',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
});

// 取消註解開啟 CodePush
// @codePush({
//   updateDialog: false,
//   installMode: codePush.InstallMode.IMMEDIATE,
// })
@connect(
  state => ({
    scene: state.routes.scene,
    loading: state.loading,
    calendar: state.calendar,
    alert: state.alert,
  }),
  dispatch => bindActionCreators({
    updateLoading,
    updateCalendar,
    updateAlert,
    updateNotifyAction,
  }, dispatch)
)
export default class AppRoute extends Component {
  constructor(props) {
    super(props);
    Text.defaultProps.allowFontScaling = false;
    this.state = {
      scenes: null,
      // onBackClick: 0
      kinship_title: '',
    };
    this.bloodClcokBtnClick = false;
    this.pressureClcokBtnClick = false;
  }

  componentWillMount() {
    fcm.init({
      updateAlert: this.props.updateAlert,
      updateNotifyAction: this.props.updateNotifyAction,
    });
  }

  async componentDidMount() {
    fcm.reopen({
      uupdateAlert: this.props.updateAlert,
      updateNotifyAction: this.props.updateNotifyAction,
    });

    NetInfo.isConnected.addEventListener(
      'change',
      this.handleConnectivityChange
    );

    const auth = await getItem(Storage.AUTHORIZATIO);
    const storageFirstUse = await getItem(Storage.FIRST_USE);
    // 開啟 intro 頁面
    // const isFirstUse = _.isEqual(storageFirstUse, {});
    const isFirstUse = false;
    const storageImportUser = await getItem(Storage.IMPORT_USER);
    const isImportUser = !_.isEqual(storageImportUser, {});
    const needLogin = Config.initPage === 'auth' || _.isEmpty(auth);
    const showLoginPage = isImportUser || (!isFirstUse && needLogin);
    // 預設進入 Home
    const showHomePage = !needLogin && !isImportUser;
    const androidSysBtn = {
      onBack: () => {
        Alert.alert(
          '',
          '取消後資料將不會被儲存，您確定要取消嗎 ?',
          [
            { text: '取消', onPress: () => {} },
            { text: '確定', onPress: () => { Actions.pop(); } },
          ],
        );
      }
    };

    this.setState({
      scenes: Actions.create(<Scene
        key="root"
        navBar={CustomNavBar}
        hideNavBar
        navigationBarStyle={{ backgroundColor: colors.whiteThree }}
      >
        <Scene key="systemLogout" hideNavBar type={ActionConst.REPLACE} component={SystemLogout} />
        <Scene key="networkFail" hideNavBar type={ActionConst.REPLACE} component={NetworkFail} />
        <Scene key="auth" type={ActionConst.REPLACE} initial >
          <Scene key="login" hideNavBar component={Login} type={ActionConst.REPLACE} title="Login" initial={showLoginPage} />
        </Scene>
        <Lightbox key="main" initial={showHomePage}>
          <Drawer
            initial={showHomePage}
            key="mainDrawer"
            contentComponent={DrawerContent}
            drawerBackgroundColor="rgba(255, 255, 255, 0.9)"
            hideNavBar
            drawerWidth={Screen.moderateScale(300)}
            drawerOpenRoute="DrawerOpen"
            drawerCloseRoute="DrawerClose"
            drawerToggleRoute="DrawerToggle"
          >
            <Stack
              key="home"
              initial={showHomePage}
              transitionConfig={() => ({ screenInterpolator: CardStackStyleInterpolator.forHorizontal })}
            >
              <Scene
                drawer
                hideNavBar
                initial={showHomePage}
                hideNavBar={false}
                key="homeIndex"
                component={Home}
                title=""
                renderRightButton={() => (
                  <TouchableOpacity
                    onPress={() => {}}
                    style={{ flexDirection: 'row', marginRight: Screen.moderateScale(9), }}
                  >
                    <H3 style={{ color: '#fff', paddingLeft: Screen.moderateScale(2) }}>按鈕</H3>
                  </TouchableOpacity>
                )}
              />
              <Scene
                key="testPage"
                hideNavBar={false}
                component={Gec}
              />
            </Stack>
            <Scene
              drawer
              hideNavBar={false}
              key="testPage2"
              component={Gec}
              title="裝置"
            />
          </Drawer>
        </Lightbox>
        <Lightbox>
          <Scene key="sandbox" initial={false} navBar={CustomNavBar} >
            <Scene key="sandboxIndex" component={SandBox} title="Sand Box" />
            <Scene key="gec" cancel component={Gec} title="GEC 通用元件" />
            <Scene key="gec3" component={Gec3} title="GEC 通用元件" />
            <Drawer
              drawer
              key="drawer"
              contentComponent={DrawerContent}
              drawerBackgroundColor="rgba(239,239,244,0.5)"
              drawerWidth={300}
              drawerOpenRoute="DrawerOpen"
              drawerCloseRoute="DrawerClose"
              drawerToggleRoute="DrawerToggle"
            >
              <Scene hideNavBar key="gec2" component={Gec2} title="GEC 通用元件2" />
            </Drawer>
            <Scene key="imagePick" component={ImagePick} title="Image Pick" />
            <Scene key="calendar" component={Calendar} title="Calendar" />
            <Scene key="text" component={PrivateTextPlace} title="Text" />
            <Scene key="inputBoxPlace" component={InputBoxPlace} title="Input Box Place" />
            <Scene key="fcmPlace" component={FCMPlace} title="FCM" />
            <Scene key="animateList" component={AnimateList} hideNavBar title="Hello" />
            <Scene key="animateDetail" component={AnimateDetail} hideNavBar title="Hello" />
            <Scene key="timePick" component={TimePick} title="Time Pick" />
            <Scene key="tabbar" tabs >
              <Scene key="tab1" title="Tab #1" icon={TabIcon} navigationBarStyle={{ backgroundColor: 'red' }} titleStyle={{ color: 'white' }}>
                <Scene key="tab1_1" component={TabView} title="Tab #1_1" onRight={() => alert('Right button')} rightTitle="Right" />
                <Scene key="tab1_2" component={TabView} title="Tab #1_2" titleStyle={{ color: 'black' }} />
              </Scene>
              <Scene key="tab2" initial title="AAA" icon={TabIcon}>
                <Scene key="tab2_1" component={TabView} title="Tab #2_1" navigationBarStyle={{ backgroundColor: 'rgba(0,0,0,0)' }} onLeft={() => alert('Left button!')} leftTitle="Left" />
                <Scene key="tab2_2" component={TabView} title="Tab #2_2" />
              </Scene>
              <Scene key="tab3" component={TabView} title="Tab #3" hideTabBar icon={TabIcon} />
              <Scene key="tab4" component={TabView} title="Tab #4" hideNavBar icon={TabIcon} />
              <Scene key="tab5" component={TabView} title="Tab #5" icon={TabIcon} />
            </Scene>
          </Scene>
          <Scene key="modal" component={AnimateDetail} />
        </Lightbox>
        <Scene key="intro" type={ActionConst.REPLACE} initial={isFirstUse}>
          <Scene key="introSlide" hideNavBar component={Intro} type={ActionConst.REPLACE} />
        </Scene>
      </Scene>)
    });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.scene.sceneKey !== this.props.scene.sceneKey) {
      this.props.updateLoading(false);
    }
  }

  componentWillUpdate(nextProps, nextState) {
    if (JSON.stringify(this.props.calendar) !== JSON.stringify(nextProps.calendar)) {
      if (nextProps.calendar.isOpen === true && this.calendarPicker) {
        this.calendarPicker.open();
      }
      if (this.props.calendar.reset !== nextProps.calendar.reset && this.calendarPicker) {
        this.calendarPicker.resetDate();
      }
    }
    if (nextProps.alert.desc !== this.props.alert.desc) {
      this.dropdown.alertWithType('info', nextProps.alert.title, nextProps.alert.desc);
    }
    if (nextProps.alert.status !== this.props.alert.status &&
      nextProps.alert.status === 'show'
    ) {
      this.dropdown.alertWithType('info', nextProps.alert.title, nextProps.alert.desc);
    }
  }

  handleConnectivityChange = async (isConnected) => {
    // 跳轉畫面
    // Actions.home({ type: 'reset' });
    const auth = await getItem(Storage.AUTHORIZATIO);
    const needLogin = _.isEmpty(auth);
    if (!needLogin) {
      if (!isConnected) {
        Actions.networkFail();
      } else {
        Actions.main({ type: 'reset' });
      }
    }
  }

  backAndroidHandler = () => {
    let dontPop = false;
    if (this.props.scene.checkTimePicker) {
      dontPop = this.props.calendar.isOpen;
      this.calendarPicker.close();
    }
    const exitAlert = () => {
      Alert.alert(
        '',
        '是否要關閉 APP ?',
        [
          { text: '取消', onPress: () => {} },
          { text: '確定', onPress: () => { BackAndroid.exitApp(); } },
        ],
      );
    };
    this.setState({ onBackClick: this.state.onBackClick + 1 });
    if (this.state.onBackClick > 1) {
      exitAlert();
    }
    setTimeout(() => {
      this.setState({ onBackClick: 0 });
    }, 500);
    if ([
      'contactUs'
    ].indexOf(this.props.scene.sceneKey) >= 0) {
      Alert.alert(
        '',
        '取消後資料將不會被儲存，您確定要取消嗎 ?',
        [
          { text: '取消', onPress: () => {} },
          { text: '確定', onPress: () => { Actions.pop(); } },
        ],
      );
    } else if (this.props.scene.sceneKey === 'auth') {
      exitAlert();
    } else if (!dontPop) {
      Actions.pop();
    }
    return true;
  }

  render() {
    if (this.state.scenes) {
      return (
        <View style={{ flex: 1 }}>
          <View style={[styles.loadingContent, this.props.loading.isOpen ? {} : { height: 0, width: 0 }]}>
            <View style={[styles.loadingCover, this.props.loading.isOpen ? {} : { height: 0, width: 0 }]}>
              {
                this.props.loading.isOpen ?
                  <ActivityIndicator
                    size="large"
                    color="white"
                  /> : null
              }
            </View>
          </View>
          <CustomRouter
            sceneStyle={{
              flex: 1,
              backgroundColor: '#fff',
              shadowColor: null,
              shadowOffset: null,
              shadowOpacity: null,
              shadowRadius: null,
            }}
            scenes={this.state.scenes}
            backAndroidHandler={this.backAndroidHandler}
            onExitApp={() => true}
          />
          <CalendarPicker
            ref={ref => this.calendarPicker = ref}
            onConfirmClick={(data) => {
              const selectedStartDate = data.selectedStartDate ? moment(data.selectedStartDate).format('YYYY-MM-DD 00:00:00') : null;
              const selectedEndDate = data.selectedEndDate ? moment(data.selectedEndDate).format('YYYY-MM-DD 23:59:59') : null;
              this.props.updateCalendar({
                isOpen: false,
                selectedStartDate,
                selectedEndDate,
              });
            }}
            onClose={() => {
              this.props.updateCalendar({
                isOpen: false,
              });
            }}
          />
          <DropdownAlert
            replaceEnabled
            closeInterval={6000}
            ref={ref => this.dropdown = ref}
            onClose={(data) => {
              this.props.updateAlert({
                title: '',
                desc: '',
                status: 'hide',
                type: '',
                service: '',
                actionSubject: '',
                actionTime: '',
              });
            }}
            onPress={async () => {
              const {
                desc, type, service, title, actionSubject, actionTime
              } = this.props.alert;
              handleNotify.action({
                type,
                title,
                service,
                updateNotifyAction: this.props.updateNotifyAction,
                actionSubject,
                actionTime,
                message: desc
              });
            }}
            infoColor={colors.subBlue}
            updateStatusBar={Platform.OS === 'ios'}
            titleStyle={{
              fontSize: Screen.moderateScale(16),
              textAlign: 'left',
              fontWeight: 'bold',
              color: 'white',
              backgroundColor: 'transparent',
              ...Platform.select({
                android: {
                }
              }),
            }}
            messageStyle={{
              fontSize: Screen.moderateScale(14),
              textAlign: 'left',
              fontWeight: 'bold',
              color: 'white',
              backgroundColor: 'transparent',
            }}
            imageStyle={{
              height: 0,
              width: 0,
            }}
          />
        </View>
      );
    }
    return null;
  }
}
