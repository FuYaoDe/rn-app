import React, { Component } from 'react';
import { Router, Reducer } from 'react-native-router-flux';
import { connect } from 'react-redux';

class CustomRouter extends Component {
  reducerCreate(params) {
    const defaultReducer = new Reducer(params);
    return (state, action) => {
      this.props.dispatch(action);
      return defaultReducer(state, action);
    };
  }

  render() {
    return (
      <Router
        createReducer={this.reducerCreate.bind(this)}
        {...this.props}
        sceneStyle={{
          flex: 1,
          backgroundColor: 'red',
          shadowColor: null,
          shadowOffset: null,
          shadowOpacity: null,
          shadowRadius: null,
        }}
      />
    );
  }
}

export default connect()(CustomRouter);
