import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TextInput
} from 'react-native';
import fcm from '../../utils/fcm';
import FCM, {
  FCMEvent,
  RemoteNotificationResult,
  WillPresentNotificationResult,
  NotificationType,
} from 'react-native-fcm';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default class FCMPlace extends Component {
  static propTypes = {};

  constructor(props) {
    super(props);
    this.state = {
      token: ''
    };
  }

  async componentDidMount() {
    fcm.init({
      updateAlert: () => {},
      updateNotifyAction: () => {},
    });
    this.setState({
      token: await fcm.getToken(),
    });
    // FCM.getFCMToken().then(token => {
    //     console.log(token)
    //     // store fcm token in your server
    // });
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>FCM</Text>
        <Text>{this.state.token}</Text>
        <TextInput style={{ width: '100%', height: 100 }} value={this.state.token} />
      </View>
    );
  }
}
