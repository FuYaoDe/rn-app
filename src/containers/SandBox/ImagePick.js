import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Image
} from 'react-native';
import ImagePicker from 'react-native-image-picker';
import RoundButton from '../../components/Button';


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    height: 100,
    width: 100,
    marginBottom: 20,
  }
});

export default class ImagePick extends Component {
  static propTypes = {};

  constructor(props) {
    super(props);
    this.state = {
      avatarSource: {}
    };
  }

  upload = () => {
    const options = {
      cancelButtonTitle: '取消',
      title: '選擇大頭貼',
      takePhotoButtonTitle: '拍照',
      chooseFromLibraryButtonTitle: '從相簿中選擇',
      customButtons: [],
      storageOptions: {
        cameraRoll: true,
        skipBackup: true,
        path: 'images'
      },
      quality: 0.6
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = { uri: response.uri };

        this.setState({
          avatarSource: source
        });
      }
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <Image source={this.state.avatarSource} style={styles.image} />
        <RoundButton text="選擇圖片" onPress={this.upload} />
      </View>
    );
  }
}
