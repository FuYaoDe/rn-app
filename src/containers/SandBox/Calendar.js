import React, { PropTypes, Component } from 'react';
import {
  TouchableOpacity,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import CalendarPicker from '../../widget/CalendarPicker';
import colors from '../../config/color';

const styles = StyleSheet.create({
  page: {
    flex: 1
  }
});

export default class Calendar extends Component {
  static propTypes = {
    onConfirmClick: PropTypes.function
  };

  static defaultProps = {
    onConfirmClick: () => {}
  };

  constructor(props) {
    super(props);
    this.state = {};
  }

  openCalendarPicker = () => {
    if (this.calendarPicker !== null) {
      this.calendarPicker.open();
    }
  };

  render() {
    return (
      <View style={styles.page}>
        <Text>
          adjklafjdljafl
          adjklafjdljafl
          adjklafjdljafl
          adjklafjdljafl
          adjklafjdljafl
          adjklafjdljafl
          adjklafjdljafl
          adjklafjdljafl
          adjklafjdljafl
          adjklafjdljafl
          adjklafjdljafl
        </Text>
        <Text>
          adjklafjdljafl
          adjklafjdljafl
          adjklafjdljafl
          adjklafjdljafl
          adjklafjdljafl
          adjklafjdljafl
          adjklafjdljafl
          adjklafjdljafl
          adjklafjdljafl
          adjklafjdljafl
          adjklafjdljafl
        </Text>

        <TouchableOpacity onPress={this.openCalendarPicker}>
          <Text>OPEN Calendar Picker</Text>
        </TouchableOpacity>
        <CalendarPicker ref={ref => this.calendarPicker = ref} paddingBottom={62} />
      </View>
    );
  }
}
