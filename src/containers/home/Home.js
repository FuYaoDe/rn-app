import React, { Component, PropTypes } from 'react';
import { StyleSheet, View } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import config from '../../config/index';
import colors from '../../config/color';
import images from '../../config/image';
import { apifetch, apiHandler } from '../../utils/api';
import { apiAction } from '../../config/api';
import { updateLoading } from '../../actions/loading';
import { Title, H3 } from '../../widget/Label';
import Screen from '../../utils/screen';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-around',
    alignItems: 'center',
    padding: Screen.moderateScale(40),
  }
});

@connect(
  state => ({
    services: state.services,
    banner: state.banner,
  }),
  dispatch => bindActionCreators({
    updateLoading,
  }, dispatch)
)
export default class Home extends Component {
  static propTypes = {};

  constructor(props) {
    super(props);
    this.state = {};
  }

  async componentDidMount() {
    this.props.updateLoading(true);
    await Promise.all([
      // this.getUserInfo(),
    ]);
    this.props.updateLoading(false);
  }


  getUserInfo = async () => {
    const res = await apifetch(apiAction.USER_INFO);
    await apiHandler({
      res,
      done: async(res) => {
    },
      fail: () => {
      }
    });
  }


  render() {
    return (
      <View style={styles.container}>
        <View>
          <H3>使用 Screen.moderateScale(int)</H3>
          <H3>H1 ~ H4 確保不同螢幕大小比例相同</H3>
        </View>
      </View>
    );
  }
}
