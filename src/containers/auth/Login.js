import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  KeyboardAvoidingView,
  Keyboard,
  Animated,
  Platform,
  StatusBar,
} from 'react-native';
import Egg from 'react-native-egg';
import { Actions } from 'react-native-router-flux';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { updateLoading } from '../../actions/loading';
import { updateNotifyAction } from '../../actions/notifyAction';
import { PrimaryBtn } from '../../widget/Button';
import { PrimaryInput } from '../../widget/InputBox';
import Storag from '../../config/storage';
import { getItem, setItem, removeItem } from '../../utils/asyncStorage';
import Screen from '../../utils/screen';
import { checkForm } from '../../utils/form';
import colors from '../../config/color';
import { Title } from '../../widget/Label';
import i18n, { i18nKey } from '../../utils/i18n';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    paddingLeft: Screen.moderateScale(40),
    paddingRight: Screen.moderateScale(40),
    backgroundColor: colors.backgroundColor,
  },
  titleContainer: {
    flex: Screen.virtualBar ? 1.6 : 2,
    // flex: 2,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: Screen.moderateScale(25),
  },
  errorMsg: { fontSize: Screen.moderateScale(12), color: colors.pink }
});

@connect(
  state => ({
    signupData: state.signup,
  }),
  dispatch => bindActionCreators({
    updateLoading,
    updateNotifyAction
  }, dispatch)
)
export default class Login extends Component {
  static propTypes = {};

  constructor(props) {
    super(props);
    this.state = {
      identifier: '',
      password: '',
      remember: false,
      isLoginBtnOnPress: false,
      showError: false,
    };
    this.logoHeight = new Animated.Value(Screen.moderateScale(49.5 * 2));
    this.logoWidth = new Animated.Value(Screen.moderateScale(135.5 * 2));
    this.backgroundImagePosition = new Animated.Value(0);
    this.notificationListener = null;
  }


  componentWillMount() {
  }

  componentDidMount() {
    this.getLoginInfo().done();
    this.keyboardShowSub = Keyboard.addListener(Screen.keyboardShow, this.keyboardShow);
    this.keyboardHideSub = Keyboard.addListener(Screen.keyboardHide, this.keyboardHide);
  }

  componentWillUnmount() {
    this.keyboardShowSub.remove();
    this.keyboardHideSub.remove();
    if (this.notificationListener) {
      this.notificationListener.remove();
    }
    if (Platform.OS === 'ios') {
      // NotificationsIOS.removeEventListener('notificationOpened');
    }
  }

  getLoginInfo = async () => {
    // const data = await getItem(Storag.LOGIN_INFO);
    // if (data.remember) {
    //   this.setState(data);
    // }
  }

  setLoginInfo = async () => {
    await setItem(Storag.LOGIN_INFO, this.state);
  }

  keyboardShow = (event) => {
    if (Platform.OS === 'ios') {
      Animated.timing(this.logoHeight, {
        toValue: Screen.moderateScale(124 * 0),
      }).start();
      Animated.timing(this.logoWidth, {
        toValue: Screen.moderateScale(146 * 0),
      }).start();

      Animated.timing(this.backgroundImagePosition, {
        toValue: 200,
      }).start();
    }
  }

  keyboardHide = () => {
    if (Platform.OS === 'ios') {
      Animated.timing(this.logoHeight, {
        toValue: Screen.moderateScale(49.5 * 2),
      }).start();
      Animated.timing(this.logoWidth, {
        toValue: Screen.moderateScale(135.5 * 2),
      }).start();

      Animated.timing(this.backgroundImagePosition, {
        toValue: 0,
      }).start();
    }
  }

  removeLoginInfo = async () => {
    await removeItem(Storag.LOGIN_INFO);
  }

  loginBtnOnPress= async () => {
    // 紀錄登入資訊
    // await this.setLoginInfo();

    this.props.updateLoading(true);
    // const res = await apifetch(apiAction.LOGIN, {
    //   pin: this.state.identifier,
    //   platform: Platform.OS === 'ios' ? 'IOS_FCM' : 'ANDROID',
    //   deviceToken: await fcm.getToken(),
    // });
    // await apiHandler({
    //   res,
    //   done: async(res) => {
    // await setItem(Storag.AUTHORIZATIO, res.data.Authorization);
    await setItem(Storag.AUTHORIZATIO, '123123');
    Actions.main({ type: 'reset' });
    //   },
    //   fail: () => {
    // this.props.updateLoading(false);
    //     this.setState({
    //       showError: true
    //     });
    //   }
    // });
  }

  checkForm = () => {
    const checkFormStatus = checkForm([
      this.idInput,
    ]);
    this.setState({
      isLoginBtnOnPress: checkFormStatus
    });
  }

  rendetInput = (child) => {
    const KeyboardAvoidingViewProps = {
      ...Platform.select({
        ios: {
          style: { flex: 1, zIndex: 1, width: Screen.width - Screen.moderateScale(80) },
          behavior: 'padding'
        },
        android: {
          style: { flex: 1, zIndex: 1 },
          behavior: null
        }
      })
    };
    // if (Platform.OS === 'ios') {
    if (true) {
      return (
        <KeyboardAvoidingView {...KeyboardAvoidingViewProps} key="LoginKeyboardAvoidingView">
          {child}
        </KeyboardAvoidingView>
      );
    }
    return (
      <View style={{
        justifyContent: 'center', alignItems: 'center', flex: 1, zIndex: 1
      }}
      >
        {child}
      </View>
    );
  }

  render() {
    const logoStyle = {
      height: this.logoHeight,
      width: this.logoWidth,
    };

    const background = {
      height: Screen.height,
      width: Screen.width,
      position: 'absolute',
      bottom: this.backgroundImagePosition,
      // bottom: 140,
    };

    return (
      <View style={styles.container}>
        <StatusBar barStyle={Platform.OS === 'ios' ? 'dark-content' : 'light-content'} />
        <Egg
          style={styles.titleContainer}
          setps="TTTTT"
          onCatch={() => {
            Actions.sandbox();
          }}
        >
          <Title style={{ fontSize: Screen.moderateScale(40) }}>Title</Title>
        </Egg>
        {
          this.rendetInput(<View style={{ flex: 1, paddingTop: Platform.OS == 'ios' ? Screen.moderateScale(35) : 0, justifyContent: 'space-between' }}>
            <PrimaryInput
              style={{ }}
              onChangeText={identifier => this.setState({ identifier }, this.checkForm)}
              value={this.state.identifier}
              placeholder="PIN 碼"
              isRequire
              ref={ref => this.idInput = ref}
            />
            {/* <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                <DefaultText numberOfLines={1} style={styles.errorMsg}>
                  {
                    this.state.showError ? i18n.t(i18nKey.loginErrorMsg) : ''
                  }
                </DefaultText>
                <LineBtn text={i18n.t(i18nKey.loginForgetpassword)} onPress={Actions.forgetPassword}/>
              </View> */}
          </View>
          )
        }
        <View style={{
          flex: 1, justifyContent: 'center', alignItems: 'center', paddingTop: 30
        }}
        >
          <PrimaryBtn
            onPress={this.loginBtnOnPress}
            text={i18n.t(i18nKey.loginButton)}
            disablePress={!this.state.isLoginBtnOnPress}
          />
        </View>
      </View>
    );
  }
}
