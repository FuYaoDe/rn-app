import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Image,
  Text,
  ScrollView,
  StatusBar,
  PixelRatio,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { H3 } from '../widget/Label';
import Screen from '../utils/screen';
import colors from '../config/color';
import { SubBtn } from '../widget/RoundButton';
import Storag from '../config/storage';
import { getItem, setItem } from '../utils/asyncStorage';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});

export default class Intro extends Component {
  static propTypes = {};
  constructor(props) {
    super(props);
    this.state = {
      currentPage: 0
    };
  }

  onScroll(e) {
    const x = Math.floor(e.nativeEvent.contentOffset.x);
    const screenWidth = Math.floor(Screen.width);
    const currentPage = Math.floor(x / screenWidth);
    if (this.state.currentPage != currentPage) {
      this.setState({
        currentPage,
      });
    }
  }

  nextPage = () => {
    if (this.scrollView) {
      this.scrollView.scrollTo({ x: Screen.width * (this.state.currentPage + 1) });
    }
  }

  render() {
    const dot = props => (
      <View
        style={{
          backgroundColor: props.selected ? colors.greyishBrown : colors.silver,
          height: Screen.moderateScale(10),
          width: Screen.moderateScale(10),
          borderRadius: Screen.moderateScale(5),
          padding: Screen.moderateScale(1),
          margin: Screen.moderateScale(2),
        }}
      />
    );

    return (
      <View style={[styles.container, {}]}>
        <StatusBar
          //translucent
          //backgroundColor="rgba(100, 100, 100, 0.2)"
          barStyle="default"
        />
        <View style={{
 flex: 0.1, justifyContent: 'center', alignItems: 'center', flexDirection: 'row', paddingTop: 10
}}
        >
          {dot({ selected: this.state.currentPage === 0 })}
          {dot({ selected: this.state.currentPage === 1 })}
          {dot({ selected: this.state.currentPage === 2 })}
          {dot({ selected: this.state.currentPage === 3 })}
          {dot({ selected: this.state.currentPage === 4 })}
        </View>
        <ScrollView
          style={{ flex: 0.9 }}
          contentContainerStyle={{}}
          horizontal
          showsHorizontalScrollIndicator={false}
          pagingEnabled
          scrollEventThrottle={16}
          onScroll={e => this.onScroll(e)}
          ref={scrollView => this.scrollView = scrollView}
        >
          <View style={{}}>
            <View style={{ flex: 0.9 }}>
              <Image
                source=""
                style={{ width: Screen.width, height: Screen.height * 0.9 }}
                resizeMode="stretch"
              />
            </View>
            <View style={{ flex: 0.1, alignItems: 'center' }}>
              <SubBtn
                borderColor={colors.whiteThree}
                style={{ position: 'absolute' }}
                onPress={this.nextPage}
                text="下一步"
                style={{ width: Screen.width - Screen.moderateScale(80) }}
              />
            </View>
          </View>
          <View style={{}}>
            <View style={{ flex: 0.9 }}>
              <Image
                source=""
                style={{ width: Screen.width, height: Screen.height * 0.9 }}
                resizeMode="stretch"
              />
            </View>
            <View style={{ flex: 0.1, alignItems: 'center' }}>
              <SubBtn
                borderColor={colors.whiteThree}
                style={{ position: 'absolute' }}
                onPress={this.nextPage}
                text="下一步"
                style={{ width: Screen.width - Screen.moderateScale(80) }}
              />
            </View>
          </View>
          <View style={{}}>
            <View style={{ flex: 0.9 }}>
              <Image
                source=""
                style={{ width: Screen.width, height: Screen.height * 0.9 }}
                resizeMode="stretch"
              />
            </View>
            <View style={{ flex: 0.1, alignItems: 'center' }}>
              <SubBtn
                borderColor={colors.whiteThree}
                style={{ position: 'absolute' }}
                onPress={this.nextPage}
                text="下一步"
                style={{ width: Screen.width - Screen.moderateScale(80) }}
              />
            </View>
          </View>
          <View style={{}}>
            <View style={{ flex: 0.9 }}>
              <Image
                source=""
                style={{ width: Screen.width, height: Screen.height * 0.9 }}
                resizeMode="stretch"
              />
            </View>
            <View style={{ flex: 0.1, alignItems: 'center' }}>
              <SubBtn
                borderColor={colors.whiteThree}
                style={{ position: 'absolute' }}
                onPress={this.nextPage}
                text="下一步"
                style={{ width: Screen.width - Screen.moderateScale(80) }}
              />
            </View>
          </View>
          <View style={{}}>
            <View style={{ flex: 0.9 }}>
              <Image
                source=""
                style={{ width: Screen.width, height: Screen.height * 0.9 }}
                resizeMode="stretch"
              />
            </View>
            <View style={{ flex: 0.1, alignItems: 'center' }}>
              <SubBtn
                style={{ position: 'absolute' }}
                borderColor={colors.whiteThree}
                onPress={async () => {
                  //const auth = await getItem(Storag.AUTHORIZATIO);
                  await setItem(Storag.FIRST_USE, false);
                  // const needLogin = _.isEmpty(auth);
                  Actions.auth({ type: 'reset' });
                }}
                text="開始使用"
                style={{ width: Screen.width - Screen.moderateScale(80) }}
              />
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}
