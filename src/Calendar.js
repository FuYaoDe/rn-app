import React, { Component } from 'react';
import {
  StyleSheet,
  View,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import CalendarPicker from 'react-native-calendar-picker';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default class Calendar extends Component {
  static propTypes = {};

  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style={styles.container}>
        <CalendarPicker
          onDateChange={this.onDateChange}
          markedDays={['2017/5/10', '2017/5/1']}
          selectedMarkedDaysColorStyle={{ backgroundColor: 'red' }}
          selectedMarkedDaysTextColorStyle={{ color: '#fff' }}
        />
      </View>
    );
  }
}
