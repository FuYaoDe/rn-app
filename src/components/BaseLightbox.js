
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet, Animated, Dimensions, Button } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { PrimaryBtn, SubBtn, LineBtn, LogoBtn } from '../widget/Button';
import Screen from '../utils/screen';

const { height: deviceHeight, width: deviceWidth } = Dimensions.get('window');

export default class BaseLightbox extends Component {
  static propTypes = {
    children: PropTypes.any,
    horizontalPercent: PropTypes.number,
    verticalPercent: PropTypes.number,
  }

  constructor(props) {
    super(props);

    this.state = {
      opacity: new Animated.Value(0),
    };
  }

  componentDidMount() {
    Animated.spring(this.state.opacity, {
      duration: 125,
      toValue: 1,
    }).start();
  }

  closeModal = ({ type = 'close' }) => {
    Animated.spring(this.state.opacity, {
      duration: 125,
      toValue: 0,
    }).start(() => {
      if (type === 'submit') {
        this.props.onSubmit();
      }
      Actions.pop();
    });
  }

  _renderLightBox = () => {
    const {
      children, horizontalPercent = 1, verticalPercent = 1, closeText, submitText
    } = this.props;
    const height = this.props.height || verticalPercent ? deviceHeight * verticalPercent : deviceHeight;
    const width = this.props.width || horizontalPercent ? deviceWidth * horizontalPercent : deviceWidth;
    return (
      <View
        style={{
          width,
          height,
          // justifyContent: 'center',
          // alignItems: 'center',
          backgroundColor: 'white',
          padding: Screen.moderateScale(30),
          paddingBottom: Screen.moderateScale(50),
        }}
      >
        <View style={{ flex: 8 }}>
          {children}
        </View>
        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center' }}>
          <PrimaryBtn
            style={{ marginRight: Screen.moderateScale(30) }}
            onPress={() => { this.closeModal({ type: 'submit' }); }}
            text={submitText}
          />
          <SubBtn
            onPress={() => { this.closeModal({ type: 'close' }); }}
            text={closeText}
          />
        </View>
      </View>
    );
  }

  render() {
    return (
      <Animated.View style={[styles.container, { opacity: this.state.opacity }]}>
        {this._renderLightBox()}
      </Animated.View >
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'rgba(52,52,52,0.3)',
    position: 'absolute',
    top: -(deviceHeight / 2 - 100),
    bottom: 0,
    left: 0,
    right: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
