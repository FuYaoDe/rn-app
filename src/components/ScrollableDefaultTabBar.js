import NotifyBox from './NotifyBox';
import colors from '../config/color';
import Screen from '../utils/screen';
import { DefaultText } from '../widget/Label';

const React = require('react');
const ReactNative = require('react-native');

const {
  StyleSheet,
  Text,
  View,
  Animated,
  ViewPropTypes
} = ReactNative;
const Button = require('./ScrollableButton');

const styles = StyleSheet.create({
  tab: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingBottom: Screen.moderateScale(10),
  },
  flexOne: {
    flex: 1,
  },
  tabs: {
    height: Screen.moderateScale(50),
    flexDirection: 'row',
    justifyContent: 'space-around',
    borderWidth: 1,
    borderTopWidth: 0,
    borderLeftWidth: 0,
    borderRightWidth: 0,
    borderColor: '#ccc',
  },
});

const DefaultTabBar = React.createClass({
  propTypes: {
    goToPage: React.PropTypes.func,
    activeTab: React.PropTypes.number,
    tabs: React.PropTypes.array,
    backgroundColor: React.PropTypes.string,
    activeTextColor: React.PropTypes.string,
    inactiveTextColor: React.PropTypes.string,
    textStyle: Text.propTypes.style,
    tabStyle: ViewPropTypes.style,
    renderTab: React.PropTypes.func,
    underlineStyle: ViewPropTypes.style,
  },

  getDefaultProps() {
    return {
      activeTextColor: 'navy',
      inactiveTextColor: 'black',
      backgroundColor: null,
    };
  },

  renderTabOption(name, page) {
  },

  renderTab(name, page, isTabActive, onPressHandler) {
    const {
      activeTextColor, inactiveTextColor, textStyle, notify
    } = this.props;
    const textColor = isTabActive ? activeTextColor : inactiveTextColor;
    const fontWeight = isTabActive ? 'bold' : 'normal';
    const data = JSON.parse(name);
    return (
      <NotifyBox
        amount={data.notify}
        color={colors.pink}
        animated="pop"
        bottom={Screen.moderateScale(30)}
        right={Screen.moderateScale(50)}
        small
      >
        <Button
          style={styles.flexOne}
          key={name}
          accessible
          accessibilityLabel={name}
          accessibilityTraits="button"
          onPress={() => onPressHandler(page)}
        >
          <View style={[styles.tab, this.props.tabStyle, ]}>
            <DefaultText style={[{ color: textColor, fontWeight, }, textStyle, ]}>
              {data.title}
            </DefaultText>
          </View>
        </Button>
      </NotifyBox>
    );
  },

  render() {
    const containerWidth = this.props.containerWidth;
    const numberOfTabs = this.props.tabs.length;
    const tabUnderlineStyle = {
      position: 'absolute',
      width: containerWidth / numberOfTabs,
      height: 4,
      backgroundColor: 'navy',
      bottom: 0,
    };

    const left = this.props.scrollValue.interpolate({
      inputRange: [0, 1, ], outputRange: [0, containerWidth / numberOfTabs, ],
    });
    return (
      <View style={[styles.tabs, { backgroundColor: this.props.backgroundColor, }, this.props.style, ]}>
        {this.props.tabs.map((name, page) => {
          const isTabActive = this.props.activeTab === page;
          const renderTab = this.props.renderTab || this.renderTab;
          return renderTab(name, page, isTabActive, this.props.goToPage);
        })}
        <Animated.View style={[tabUnderlineStyle, { left, }, this.props.underlineStyle, ]} />
      </View>
    );
  },
});

module.exports = DefaultTabBar;
