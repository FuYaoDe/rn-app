import React from 'react';
import { AppRegistry, View } from 'react-native';
import { Provider } from 'react-redux';
import AppRoute from './containers/AppRoute';
import configureStore from './configureStore';
import config from './config';

console.disableYellowBox = config.disableYellowBox;

const store = configureStore();

const RNBoilerplate = () => (
  <Provider store={store}>
    <AppRoute />
  </Provider>
);

AppRegistry.registerComponent('RNBoilerplate', () => RNBoilerplate);
